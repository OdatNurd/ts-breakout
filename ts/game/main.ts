module nurdz.main
{
    /**
     * Set up the button on the page to toggle the state of the game.
     *
     * @param stage the stage to control
     * @param buttonID the ID of the button to mark up to control the game state
     */
    function setupButton (stage, buttonID)
    {
        // True when the game is running, false when it is not. This state is toggled by the button. We
        // assume that the game is going to start running.
        var gameRunning = true;

        // Get the button.
        var button = document.getElementById (buttonID);
        if (button == null)
            throw new ReferenceError ("No button found with ID '" + buttonID + "'");

        // Set up the button to toggle the stage.
        button.addEventListener ("click", function ()
        {
            // Try to toggle the game state. This will only throw an error if we try to put the game into
            // a state it is already in, which can only happen if the engine stops itself when we didn't
            // expect it.
            try
            {
                if (gameRunning)
                {
                    stage.muteMusic (true);
                    stage.muteSounds (true);
                    stage.stop ();
                }
                else
                {
                    stage.muteMusic (false);
                    stage.muteSounds (false);
                    stage.run ();
                }
            }

            // Log and then re-throw the error.
            catch (error)
            {
                console.log ("Exception generated while toggling game state");
                throw error;
            }

            // No matter what, toggle the game state. This will put the button back into sync for the next
            // click if it got out of sync.
            finally
            {
                // No matter what, toggle the state.
                gameRunning = !gameRunning;
                button.innerHTML = gameRunning ? "Stop Game" : "Restart Game";
            }
        });
    }

    // Once the DOM is loaded, set things up.
    nurdz.contentLoaded (window, function ()
    {
        try
        {
            // Set up the stage.
            var stage = new game.Stage('gameContent', 'black', true, '#a0a0a0');

            // Set up the default values used for creating a screen shot.
            game.Stage.screenshotFilenameBase = "ts-breakout";
            game.Stage.screenshotWindowTitle = "ts-breakout";

            // Set up the button that will stop the game if something goes wrong.
            setupButton (stage, "controlBtn");

            // Preload all of our music values.
            game.music = [];
            game.music[game.GameMusic.MUSIC_TITLE]    = stage.preloadMusic ("PopcornSliderLoop");
            game.music[game.GameMusic.MUSIC_GAME]     = stage.preloadMusic ("Ouroboros");
            game.music[game.GameMusic.MUSIC_GAMEOVER] = stage.preloadMusic ("Mellowtron");

            // Set up the sprite sheet for the global speaker icon.
            game.speakerSheet = new game.SpriteSheet (stage, "speaker_2_1.png", 2, 1, true);

            // Try to set the high score and the music mute state to their last stored value; these will end
            // up with default values (e.g. 0 or false) if there is currently no stored value or if the local
            // storage mechanism is not enabled/present.
            game.highScore = game.loadHighScore();
            game.musicMute = game.loadMusicMute ();

            // Register all of our scenes.
            stage.addScene ("title", new game.TitleScene (stage));
            let gameScene = new game.GameScene (stage);
            stage.addScene ("game", gameScene);
            stage.addScene ("gameOver", new game.GameOverScene (stage, gameScene));

            // Switch to the initial scene and run the game.
            stage.switchToScene ("title");
            stage.run ();
        }
        catch (error)
        {
            console.log ("Error starting the game");
            throw error;
        }
    });
}
