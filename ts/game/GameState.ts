module nurdz.game
{
    /**
     * This specifies the number of points that need to be scored in order to earn an extra life.
     *
     * This value is treated as cumulative with itself.
     *
     * @type {Number}
     */
    export const EXTRA_LIFE_SCORE = 30000;

    /**
     * The storage object in the browser that we should use to store persistent information; the value here
     * is null if local storage is not supported on this device or if it is but is currently turned off (e.g.
     * private browsing).
     *
     * @type {Storage}
     */
    var storage: Storage = getLocalStorage();

    /**
     * The current score of the game. This increases every time the ball removes a brick or bricks from the
     * play area. This resets at the start of a new game.
     *
     * @type {number}
     */
    export var score: number = 0;

    /**
     * The highest score that has been achieved so far; this gets stored in local storage (if the browser
     * supports it) and restored at the start of the next session.
     *
     * When it's 0, the high score is not reported anywhere.
     *
     * @type {number}
     */
    export var highScore: number = 0;

    /**
     * Whether or not music should be muted or not; this gets stored in local storage (if the browser
     * supports it) and restored at the start of the next session so that the mute state tracks from one
     * session to another.
     *
     * @type {boolean}
     */
    export var musicMute: boolean = false;

    /**
     * The number of "lives" the player has. This decreases by one every time the ball falls off the bottom of
     * the screen and resets. When this hits 0, everything resets back to its initial state and the game goes
     * again.
     *
     * @type {number}
     */
    export var lives: number = 3;

    /**
     * A sprite sheet that has two sprites in it; one that shows a regular speaker and one that shows a muted
     * speaker. Scenes can render this selecting the sprite to use based on the current mute state in order to
     * visually represent the mute state of the game.
     *
     * @type {SpriteSheet}
     */
    export var speakerSheet : SpriteSheet;

    /**
     * This is an array of the colors that are used primarily for the bricks in the game. This is used to
     * construct a gradient for the title text that will allow us to use all of the colors at once as well as
     * other uses.
     *
     * @type {Array<string>}
     */
    export var brickColors: Array<string> = ['#494949', '#e5e5e5', '#c83e3e', '#e86a17',
        '#ffcc00', '#80be1f', '#1ea7e1', '#ff99cc'];

    /**
     * This defines the different types of music that all of the scenes share with each other. The values in
     * the enumeration are used as array indexes to select the appropriate global music.
     *
     * @type {[type]}
     */
    export enum GameMusic
    {
        MUSIC_TITLE,
        MUSIC_GAME,
        MUSIC_GAMEOVER
    };

    /**
     * An array that contains all of the preloaded music that a scene can play if it wants to. This should be
     * indexed only by values from the GameMusic enumeration.
     *
     * @type {Array<Sound>}
     */
    export var music : Array<Sound>;

    /**
     * Given a stage and an image, return a version of the image that has been tiled/constrained to the
     * size of the provided stage.
     *
     * In particular, when the image provided is exactly the dimensions of the stage, it is returned back. In
     * all other cases (including being larger than the stage) a new image is constructed using the dimensions
     * of the stage provided and the image is tiled onto it, potentially performing a crop.
     *
     * The resulting image is then returned.
     *
     * @param  stage the stage the image should take its size from
     * @param  image the image to file
     *
     * @return an image that is potentially a tiled or cropped version of the passed in image
     */
    export function tileImage (stage: Stage, image: HTMLImageElement): HTMLImageElement
    {
        // If the incoming image is already the size of the stage, it's find the way that it is.
        // If the image is fine, use it as is.
        if (image.width == stage.width && image.height == stage.height)
            return image;

        // Get the renderer from the stage.
        let renderer = <CanvasRenderer> stage.renderer;

        // Save the current contents of the canvas so that we can restore it.
        let stageContents = renderer.context.getImageData(0, 0, stage.width, stage.height);

        // Tile the image we got across the stage
        for (let blitY = 0; blitY < stage.height; blitY += image.height)
        {
            for (let blitX = 0; blitX < stage.width; blitX += image.width)
                renderer.blit(image, blitX, blitY);
        }

        // Now convert the canvas into an image; as far as I can tell this is the only way to do it.
        let tiledImg = document.createElement("img");
        tiledImg.src = stage.canvas.toDataURL("image/png");

        // Now put the original stage contents back where we got it and make sure it gets collected.
        renderer.context.putImageData(stageContents, 0, 0);
        stageContents = null;

        return tiledImg;
    }

    /**
     * Perform a check to see if the current browser supports local storage. This tests not only that the
     * feature exists, but that it is currently enabled and usable.
     *
     * @return {Boolean} true if the current browser supports local storage and it is enabled, or false
     * otherwise
     */

    /**
     * Perform a check to see if the current browser supports local storage and that it is actually enabled
     * and can save and restore data.
     *
     * If the check passes, the function returns the storage object to use; otherwise it returns null.
     *
     * @return {Storage} [description]
     */
    function getLocalStorage () : Storage
    {
        try
        {
            // Pull the local storage object from the window, if any. This has to be inside of the exception
            // handler because in some cases this can throw an exception.
            let storage: Storage = window['localStorage'];
            let tmp = '_____storage_test_____';
            let result;

            // Try to set the temporary value in using itself as a key, then extract the value.
            storage.setItem(tmp, tmp);
            result = storage.getItem(tmp) == tmp;

            // Remove the item now.
            storage.removeItem(tmp);

            // If the result is a true value, then everything worked, so return the storage object.
            if (result)
                return storage;
        }
        catch (e)
        {
        }

        // There is no support, or there is but we're size restricted.
        return null;
    }

    /**
     * Try to load a previously saved high score from local storage, if it is enabled on this browser and has
     * data stored.
     *
     * When this is not the case (no local storage, no data stored yet) the returned high score will be 0.
     *
     * @return {Number} the last known high score, or 0 if we don't have one
     */
    export function loadHighScore () : number
    {
        // Is there a storage object to fetch data from?
        if (storage != null)
        {
            // Collect the data from local storage and try to parse it into an integer. If that works, we can
            // return the value.
            let result = storage.getItem("highScore");
            if (result != null)
            {
                // Parse the string back into a number; this returns NaN for things that are not a number.
                result = parseInt(result, 10);
                if (isNaN (result) == false)
                    return result;
            }
        }

        // We can't load a high score, so just return 0 in this case to turn that feature off for the time
        // being, until a game is played locally.
        return 0;
    }

    /**
     * Persist the current high score to local storage, so that when the user returns it's still set.
     *
     * If there is no current local storage, this does nothing.
     */
    export function saveHighScore () : void
    {
        // If we have a local storage, store the current high score there.
        if (storage != null)
            storage.setItem("highScore", highScore + "");
    }

    /**
     * Try to load a previously saved mute state for music from local storage, if it is enabled on this
     * browser and has data stored.
     *
     * When this is not the case (no local storage, no data stored yet), the returned mute state will be false
     * (i.e. music will play)
     *
     * @return {boolean} the last known mute state for music, or false if we don't have one
     */
    export function loadMusicMute () : boolean
    {
        // Is there a storage item to fetch data from?
        if (storage != null)
        {
            // Local storage only stored strings, so we rely on the stored value being the exact string "true"
            // to indicate mute is on and anything else to be false.
            let result = storage.getItem ("musicMute")            ;
            if (result != null)
                return result === "true";
        }

        // We can't load the mute state so just return false to indicate that we want music for the time
        // being, until the user turns it off again (if they want to).
        return false;
    }

    /**
     * Persist the current music mute state to local storage, so that when the user returns it's still set.
     *
     * If there is no current local storage, this does nothing.
     */
    export function saveMusicMute () : void
    {
        // If we have a local storage, store the value there
        if (storage != null)
            storage.setItem ("musicMute", musicMute.toString ());
    }

    /**
     * Render the current state of music (muted or playing) onto the provided stage. This renders the
     * appropriate sprite at the appropriate location on the screen.
     *
     * @param {Stage} stage the stage to render onto
     */
    export function renderMuteState (stage : Stage) : void
    {
        // Render the mute state in the lower left corner.
        speakerSheet.blit (musicMute ? 1 : 0, 16, stage.height - 16 - speakerSheet.height, stage.renderer);
    }
}
