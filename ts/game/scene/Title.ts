module nurdz.game
{
    /**
     * The title of the game as displayed on the title screen.
     *
     * @type {String}
     */
    const TITLE_TEXT = "Super Brick Breaker";

    /**
     * The blinking text that tells the player how to play.
     *
     * @type {String}
     */
    const CLICK_TEXT = "Click to play";

    /**
     * The text that is used to introduce the high score, if one is set.
     *
     * @type {String}
     */
    const HIGH_SCORE_TEXT = "High Score: ";

    /**
     * The font used to render the title of the game.
     *
     * @type {String}
     */
    const TITLE_FONT = '48px kenvector_futureregular';

    /**
     * The font used to display the "click to play" message.
     *
     * @type {String}
     */
    const PLAY_FONT = '16px kenvector_futureregular';

    /**
     * The "score" font; this is the font that is used to display the high score
     * text (if any).
     *
     * @type {String}
     */
    const SCORE_FONT = '30px kenvector_futureregular';

    /**
     * The "normal" font; this is used for everything not outlined above.
     *
     * @type {String}
     */
    const NORMAL_FONT = '20px kenvector_futureregular';

    /**
     * All of the text that appears on the title screen, apart from the
     * notification that you should click to play, the high score, or the name
     * of the game.
     *
     * @type {Array<String>}
     */
    const infoText: Array<string> = [
        "A simple breakout clone",
        "",
        "Based on the course by Chris DeLeon (@ChrisDeleon)",
        "See http://how-to-program-games.com/ for more",
        "",
        "This version is \xA9 2016 Terence Martin (@OdatNurd)",
        "",
        "http://bloggity.nurdz.com/",
        "http://gamedev.nurdz.com/",
        "http://gitlab.com/u/OdatNurd/"
    ];

    /**
     * This scene represents the game screen, where the game is actually played.
     */
    export class TitleScene extends Scene
    {
        /**
         * Override the type of our renderer to be a canvas renderer.
         */
        protected _renderer : CanvasRenderer;

        /**
         * The last known position of the mouse on the stage.
         */
        private _mouse : Point;

        /**
         * The image that we use on the background of the game screen.
         */
        private _backgroundImg: HTMLImageElement;

        /**
         * An entity that displaying blinking text and tells the user to press click to start a game.
         */
        private _clickText: BlinkingText;

        /**
         * The gradient that is used to display the title of the game. This uses the colors from brickColors
         * to make the text a rainbow.
         *
         * @type {CanvasGradient}
         */
        private _gradient: CanvasGradient;

        /**
         * Construct a new game screen scene that will display on the provided stage.
         *
         * @param stage the stage the scene will present on
         */
        constructor (stage : Stage)
        {
            super ("titleScreen", stage);

            // Create a point that will store the mouse location.
            this._mouse = new Point (0, 0);

            // Some blinking text that tells the user how to start the game. We want this to be centered on
            // the screen a little ways from the bottom
            this._clickText = new BlinkingText(stage, PLAY_FONT, CLICK_TEXT);
            this._clickText.position.setToXY(stage.width / 2, stage.height - 25);

            // Add the item to the screen so that it will render properly.
            this.addActor(this._clickText);

            // Preload the image for the background; we don't assign this to an image directly, because after
            // loading we might need to tile the image.
            stage.preloadImage("blue_background_tile.png", this.setBackgroundImg);

            // Set up the gradient we use to display our title.
            this._gradient = this.makeTitleGradient();
        }

        /**
         * Create the gradient that will be used to display the title text. This uses the colors from the
         * _brickColors array and returns back a gradient that can be used to render the title text.
         *
         * Note that due to the way that gradients work in HTML canvas, this sets up the gradient to assume
         * that a translate to the center of the canvas is going to happen when the title text is rendered.
         *
         * @return {CanvasGradient} the gradient used to display the title text.
         */
        private makeTitleGradient () : CanvasGradient
        {
            let titleWidth = 0;

            // A gradient covers a certain area of the canvas, so we need to determine how wide the title text
            // will be when we render it, so that we can make a gradient that covers the whole thing. We do
            // that by temporarily setting the title font so we can measure the text.
            this._renderer.context.save();
            this._renderer.context.font = TITLE_FONT;
            titleWidth = this._renderer.context.measureText(TITLE_TEXT).width;
            this._renderer.context.restore();

            // Now create the gradient itself. A linear gradient is represented by a line (specified in canvas
            // coordinates); anything to the left of the start point or the right of the end point is an
            // extrapolation of the gradient.
            //
            // An important note here is that the gradient position is always in canvas coordinates, even if
            // the canvas is translated. We want to center our title text when we render it which we do by
            // translating the origin to the center point horizontally.
            //
            // This has the unfortunate effect of making everything to the left of the center line be an
            // extension of the first color stop because it is less than 0 and thus outside the bounds of the
            // gradient.
            //
            // To get around that, when we define the gradient, we do it knowing that half of it's width will
            // be to the left of the center point and half of it to the right.
            let gradient = this._renderer.context.createLinearGradient(-(titleWidth / 2), 0, titleWidth / 2, 0);

            // Gradient color stops are set by specifying a position between 0.0 and 1.0. Determine how far
            // each color step in the gradient will be, as a percentage.
            let step = 1.0 / brickColors.length;

            // Loop setting in all of the color stops now. It is an error to specify a position that is larger
            // than 1.0, so we take care to make sure that the last gradient stop is always exactly at the 1.0
            // position, in case the number of color stops doesn't make this happen in a nicer way.
            for (let i = 0, position = 0.0; i < brickColors.length; i++, position += step)
            {
                if (i == brickColors.length - 1)
                    position = 1.0;

                // Add a color stop
                gradient.addColorStop(position, brickColors[i]);
            }

            // Return the gradient now.
            return gradient;
        }

        /**
         * This gets invoked when our scene becomes the active scene; If the music is not muted and not
         * already playing, this starts it.
         *
         * @param previousScene the scene that used to be active.
         */
        activating (previousScene : nurdz.game.Scene) : void
        {
            // Let the super do its thing.
            super.activating (previousScene);

            // Set up what our font should be while this screen is active.
            this._renderer.context.font = NORMAL_FONT;

            // Play the title screen music if we're not muted.
            if (musicMute == false)
                music[GameMusic.MUSIC_TITLE].play ();
        }

        /**
         * This gets invoked when our scene is no longer going to be the active scene; stop our music from
         * playing.
         */
        deactivating (nextScene : nurdz.game.Scene) : void
        {
            // Let the super do it's thing
            super.deactivating (nextScene);

            // Stop our music if it's playing.
            if (music[GameMusic.MUSIC_TITLE].isPlaying)
                music[GameMusic.MUSIC_TITLE].pause ();
        }

        /**
         * This is triggered whenever the mouse is released after it was pressed over the canvas.
         *
         * @param  {MouseEvent} eventObj the event that represents the mouse release
         * @return {boolean} true if we handled the event or false if not
         */
        inputMouseUp (eventObj : MouseEvent) : boolean
        {
            // When clicked, switch to the game scene to play the actual game.
            this._stage.switchToScene('game');
            return true;
        }

        /**
         * Triggers every time a key is pressed
         *
         * @param eventObj
         * @returns {boolean}
         */
        inputKeyDown(eventObj: KeyboardEvent): boolean
        {
            switch (eventObj.keyCode)
            {
                // For the F key, toggle between full screen mode and windowed mode.
                case KeyCodes.KEY_F:
                    this._stage.toggleFullscreen();
                    return true;

                // For the M key, toggle our mute state, playing or stopping music as needed
                case KeyCodes.KEY_M:
                    // Toggle the state and save the new value
                    musicMute = !musicMute;
                    saveMusicMute ();

                    // If we're now muted, pause the music, otherwise play it.
                    music[GameMusic.MUSIC_TITLE].toggle ();
                    return true;
            }

            // Let the default happen
            return super.inputKeyDown(eventObj);
        }

        /**
         * Draw the text provided centered horizontally and vertically at the position given, using a given
         * font and color/gradient.
         *
         * This operates by making a translation of the canvas origin to be the location provided and then
         * render the text at (0, 0) after setting everything up. This is important to realize when using a
         * gradient.
         *
         * The color provided can be either a color spec string or it can be a CanvasGradient object. In the
         * latter case, the gradient is used as is. Due to the way that gradients work in HTML canvas, this
         * means that you probably want to have given the gradient a width that is the same as the width of
         * the text to be rendered AND that it's position starts to the left of the origin and continues to
         * the right.
         *
         * @param x     the x location to center around
         * @param y     the y location to center around
         * @param font  the font to use, including the size
         * @param text  the text to render
         * @param color the color to blit with OR the gradient to use to render (see above)
         */
        renderCenteredAt (x : number, y : number, font : string, text : string, color : string|CanvasGradient) : void
        {
            // Translate the context so the origin is at the point we were given; this also saves the state
            // of the context.
            this._renderer.translateAndRotate (x, y);

            // Alter the font and text alignment so that text is centered horizontally and vertically around
            // the given position/
            this._renderer.context.font = font;
            this._renderer.context.textAlign = "center";
            this._renderer.context.textBaseline = "middle";

            // If the color parameter is not a string, then it must be a gradient. In this case we will set
            // the fill style of the context and then set the color string to be undefined. This means that
            // the call to drawTxt() below will not set a text color, which would override the fill style.
            //
            // This should actually be fixed in the renderer, but no time for that now.
            if (typeof color != "string")
            {
                this._renderer.context.fillStyle = color;
                color = undefined;
            }

            // Render the text with the color given (if any) and then restore the context. Here we need to
            // use a typecast to tell the typescript compiler that color is a string.
            this._renderer.drawTxt(text, 0, 0, <string> color);
            this._renderer.restore();
        }

        /**
         * This is called whenever our scene needs to be rendered.
         */
        render () : void
        {
            // Clear the screen with our background image.
            this._renderer.blit(this._backgroundImg, 0, 0);

            // Draw the title of the game.
            this.renderCenteredAt(this._stage.width / 2, 25, TITLE_FONT, TITLE_TEXT, this._gradient);

            // If there is a high score set, we want to display it now; this doesn't happen if we don't have
            // a current high score, since displaying that the high score is 0 is kind of weak.
            if (highScore != 0)
                this.renderCenteredAt(this._stage.width / 2, 64, SCORE_FONT, HIGH_SCORE_TEXT + highScore, 'red');

            // Render the info text now; We will do a translate here to make things easier on us.
            this._renderer.translateAndRotate(32, 200);
            this._renderer.context.font = NORMAL_FONT;
            this._renderer.context.textBaseline = "middle";
            for (let i = 0, y = 0; i < infoText.length; i++ , y += 24)
                this._renderer.drawTxt(infoText[i], 0, y, 'white');
            this._renderer.restore();

            // Lastly, invoke the super method, which will render our entities.
            super.render();

            // Render the current mute state now, on top of everything else.
            renderMuteState (this._stage);
        }

        /**
         * Invoked when the image we want to use for our background is loaded; this sets up our member
         * variable for the background.
         *
         * When the image provided is smaller than the stage, the image is tiled to a size that will fill the
         * stage first.
         *
         * @param image The image that was loaded
         */
        setBackgroundImg = (image : HTMLImageElement): void =>
        {
            // Create a tiles version of the coming image.
            this._backgroundImg = tileImage(this._stage, image);
        }
    }
}
