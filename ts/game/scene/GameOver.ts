module nurdz.game
{
    /**
     * The font used to display the "press a key" message that restarts the game.
     *
     * @type {String}
     */
    const RESTART_FONT = '16px kenvector_futureregular';

    /**
     * The font used to render the game over text.
     *
     * @type {String}
     */
    const GAME_OVER_FONT = '64px kenvector_futureregular';

    /**
     * This scene represents the game screen, where the game is actually played.
     */
    export class GameOverScene extends Scene
    {
        /**
         * Override the type of our renderer to be a canvas renderer.
         */
        protected _renderer : CanvasRenderer;

        /**
         * A scene that we will proxy a render call to prior to doing our own rendering.
         */
        private _proxyScene: Scene;

        /**
         * An entity that tells the user to press a key to restart the game.
         */
        private _pressText: BlinkingText;

        /**
         * An entity that tells the user that the game is over.
         */
        private _gameOver: BlinkingText;

        /**
         * Construct a new game screen scene that will display on the provided stage.
         *
         * @param stage the stage the scene will present on
         */
        constructor (stage : Stage, proxy : Scene)
        {
            super ("gameOverScreen", stage);

            // Save the proxy scene we've been given.
            this._proxyScene = proxy;

            // Create some blinking text that tells us that the game is over.
            this._gameOver = new BlinkingText(stage, GAME_OVER_FONT, "Game Over");
            this._gameOver.position.setToXY(stage.width / 2, stage.height / 2);
            this._gameOver.colors = brickColors;
            this._gameOver.blinkTicks = 4;

            // Create our blinking text. We want it centered on the screen and a little ways up.
            this._pressText = new BlinkingText(stage, RESTART_FONT, "Press any key...");
            this._pressText.position.setToXY(stage.width / 2, stage.height - 25);

            // Add both to the scene now.
            this.addActor(this._gameOver);
            this.addActor(this._pressText);
        }

        /**
         * This gets invoked when our scene becomes the active scene; If the music is not muted and not
         * already playing, this starts it.
         *
         * @param previousScene the scene that used to be active.
         */
        activating (previousScene : nurdz.game.Scene) : void
        {
            // Let the super do its thing.
            super.activating (previousScene);

            // Set up what our font should be while this screen is active.
            this._renderer.context.font = '30px kenvector_futureregular';

            // Play the game over music if we're not muted.
            if (musicMute == false)
                music[GameMusic.MUSIC_GAMEOVER].play ();
        }

        /**
         * This gets invoked when our scene is no longer going to be the active scene; stop our music from
         * playing.
         */
        deactivating (nextScene : nurdz.game.Scene) : void
        {
            // Let the super do it's thing
            super.deactivating (nextScene);

            // Stop our music if it's playing.
            if (music[GameMusic.MUSIC_GAMEOVER].isPlaying)
                music[GameMusic.MUSIC_GAMEOVER].pause ();
        }

        /**
         * Triggers every time a key is pressed
         * @param eventObj
         * @returns {boolean}
         */
        inputKeyDown(eventObj: KeyboardEvent): boolean
        {
            switch (eventObj.keyCode)
            {
                // For the F key, toggle between full screen mode and windowed mode.
                case KeyCodes.KEY_F:
                    this._stage.toggleFullscreen();
                    return true;

                // For the M key, toggle our mute state, playing or stopping music as needed
                case KeyCodes.KEY_M:
                    // Toggle the state and save the new value
                    musicMute = !musicMute;
                    saveMusicMute ();

                    // If we're now muted, pause the music, otherwise play it.
                    music[GameMusic.MUSIC_GAMEOVER].toggle ();
                    return true;

                // Any other key jumps us back to the title screen.
                default:
                    this._stage.switchToScene('title');
                    return true;
            }

            // Let the default happen
            // return super.inputKeyDown(eventObj);
        }

        /**
         * This is called whenever our scene needs to be rendered.
         */
        render () : void
        {
            // Let the proxy render itself, which will give us a background and the remainder of the tiles,
            // but not the paddle and the ball because the game is over.
            this._proxyScene.render();

            // Now invoke the super method, which will make our own entities render.
            super.render ();
        }
    }
}
