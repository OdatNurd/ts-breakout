module nurdz.game
{
    /**
     * Every time a ball speed change happens, this is how much the ball speed gets kicked up as a percentage
     * of its current speed (values smaller than 1 slow it down while values of exactly 1 do nothing).
     */
    export const BALL_SPEED_INCREASE = 1.10;

    /**
     * The minimum speed of the ball to be considered for the multiplier; this corresponds to the speed the
     * ball must be traveling to earn MIN_SCORE_MULTIPLIER.
     */
    const MIN_MULTIPLIER_SPEED = 8;

    /**
     * The maximum speed of the ball to be considered for the multiplier; this corresponds to the speed the
     * ball must be traveling to earn MAX_SCORE_MULTIPLIR.
     */
    const MAX_MULTIPLIER_SPEED = 16;

    /**
     * The minimum score multiplier. This is the smallest multiplier that can be earned, and corresponds to
     * the MIN_MULTIPLIER speed.
     */
    const MIN_SCORE_MULTIPLIER = 1;

    /**
     * The maximum score multiplier. This is the largest multiplier that can be earned, and corresponds to
     * the MAX_MULTIPLIER speed.
     */
    const MAX_SCORE_MULTIPLIER = 5;

    /**
     * The maximum number of lives that can be displayed on the screen; any more than this are not displayed
     * because they would go off the side of the screen in an unfavorable way.
     */
    const MAX_LIVES_DISPLAYED = 8;

    /**
     * When one of the extra life icons is blinking, this is how many game ticks it takes for the display
     * to toggle states (there are 30 ticks per second).
     */
    const LIFE_BLINK_TICKS = 3;

    /**
     * How long (in ticks) to blink an extra life icon when that is turned on. There are 30 ticks per second.
     */
    const LIFE_BLINK_DURATION = 60;

    /**
     * This represents the different sounds that we know how to play based on events in the game. The values
     * in the enum are used as indexes into the array of sound effects at runtime.
     */
    enum GameSoundItem
    {
        BALL_LOST,
        EXTRA_BALL,
        COLLIDE_WALL,
        COLLIDE_PADDLE,
        COLLIDE_BRICK
    };

    /**
     * This scene represents the game screen, where the game is actually played.
     */
    export class GameScene extends Scene implements BallEventListener
    {
        /**
         * Override the type of our renderer to be a canvas renderer.
         */
        protected _renderer : CanvasRenderer;

        /**
         * The ball entity.
         */
        private _ball : Ball;

        /**
         * The paddle.
         */
        private _paddle : Paddle;

        /**
         * The grid of bricks that represents the current level.
         */
        private _brickGrid : BrickGrid;

        /**
         * The image that we use to show how many lives are left.
         */
        private _lifeIcon: HTMLImageElement;

        /**
         * The image that we use on the background of the game screen.
         */
        private _backgroundImg: HTMLImageElement;

        /**
         * The list of sounds that we know how to play based on events in the game. This array is indexed by
         * values of type GameSoundItem.
         */
        private _sounds : Array<Sound>;

        /**
         * The last known position of the mouse on the stage.
         */
        private _mouse : Point;

        /**
         * True if the first extra life icon rendered should be rendered as blinking, and false if it should
         * be displayed regularly. This is turned on for a brief period after an extra life is awarded to
         * call attention to the fact.
         */
        private _blinkLifeIcon : boolean;

        /**
         * The number of game ticks that are remaining before the state of _blinkLifeIcon toggles from true
         * back to false to automatically stop the blinking. This is initially set to LIFE_BLINK_DURATION when
         * the flag is turned on.
         */
        private _blinkLifeDuration : number;

        /**
         * When we are blinking an extra life icon, this is true if the current state for the blinking icon
         * is "on" and false when it is "off"; the state toggles every LIFE_BLINK_TICKS ticks.
         */
        private _blinkOn : boolean;

        /**
         * This tracks at what score the next free life will be awarded. It gets updated as new lives are
         * earned so that it's always a cumulative total of what the next total will be.
         *
         * You should set this value via its property and not this value directly because that makes sure that
         * _nextLifeText and _nextLifeTextPos are also updated as appropriate.
         */
        private _nextLifeScore : number;

        /**
         * A textual version of _nextLifeScore that is displayed to the screen to indicate at what score the
         * next life will be earned.
         *
         * This is pre-calculated so that we can calculate its width in _nextLifeTextPos for display on the
         * screen without having to do it 30 times a second.
         *
         * This gets set when you change _nextLifeScore using its set property.
         */
        private _nextLifeText : string;

        /**
         * The X coordinate of where the _nextLifeText string should be rendered on the screen. This is
         * adjusted so that the text always appears a set distance from the side of the screen regardless of
         * the number.
         *
         * This gets set when you change _nextLifeScore using its set property./
         */
        private _nextLifeTextPos : number;

        /**
         * Set the value of the score that will aware the next free life. This sets the member like you would
         * expect it to but also pre-calculates the text version and where it should be displayed.
         *
         * @param newScore the new score to set as the free man target
         */
        private set nextLifeScore (newScore : number)
        {
            // First, set the next life score.
            this._nextLifeScore = newScore;

            // Now update the next life text and text position. This requires our renderer so that it can
            // measure how wide the text will be in the current font.
            //
            // The 16 here is taken because that's how far to the right the score is displayed in the render
            // method. I didn't make a constant for that due to laziness.
            this._nextLifeText = "Next: " + this._nextLifeScore;
            this._nextLifeTextPos = this._stage.width - 16 - this._renderer.context.measureText (this._nextLifeText).width;
        }

        /**
         * Get the score at which the next life will be awarded
         *
         * @return the score at which the next life will be awarded.
         */
        private get nextLifeScore () : number { return this._nextLifeScore; }

        /**
         * Set the state of the flag that indicates if the first extra life icon (if any) should blink or not.
         *
         * Setting this to true sets up the blink to start at the next frame update and proceed for a set
         * duration, while setting it to false stops the blink.
         *
         * @param newBlink true to turn on blinking and false to turn it off.
         */
        private set blinkLifeIcon (newBlink : boolean)
        {
            // First, set the main boolean, then set up as needed.
            this._blinkLifeIcon = newBlink;
            if (newBlink)
            {
                this._blinkLifeDuration = LIFE_BLINK_DURATION;
                this._blinkOn = true;
            }
        }

        /**
         * Construct a new game screen scene that will display on the provided stage.
         *
         * @param stage the stage the scene will present on
         */
        constructor (stage : Stage)
        {
            super ("gameScreen", stage);

            // There is initially no blinking life icon.
            this.blinkLifeIcon = false;

            // Create a point that will store the mouse location.
            this._mouse = new Point (0, 0);

            // Create our entities now.
            this._ball = new Ball (stage);
            this._paddle = new Paddle (stage);
            this._brickGrid = new BrickGrid (stage);

            // Preload the image for the background; we don't assign this to an image directly, because after
            // loading we might need to tile the image.
            stage.preloadImage("blue_background_tile.png", this.setBackgroundImg);

            // Preload the image we use for our life icon.
            this._lifeIcon = stage.preloadImage("lifeIcon.png");

            // Preload all of our sounds; these are stored in an array.
            this._sounds = [];
            this._sounds[GameSoundItem.BALL_LOST]      = stage.preloadSound ("ball_lost");
            this._sounds[GameSoundItem.EXTRA_BALL]     = stage.preloadSound ("extra_life");
            this._sounds[GameSoundItem.COLLIDE_WALL]   = stage.preloadSound ("collide_wall");
            this._sounds[GameSoundItem.COLLIDE_PADDLE] = stage.preloadSound ("collide_paddle");
            this._sounds[GameSoundItem.COLLIDE_BRICK]  = stage.preloadSound ("collide_brick");

            // Turn on debug mode for all of the entities so that we can verify that everything is working
            // the way we want it to.
            // this._ball.properties.debug = true;
            // this._paddle.properties.debug = true;

            // Add all entities as actors so that they get updated and rendered. Note that we add the
            // paddle first; this means that it gets updates and renders first, which means that it
            // renders below the ball but also that it moves before the ball moves.
            this.addActor (this._paddle);
            this.addActor (this._ball);
            this.addActor (this._brickGrid);

            // Set ourselves as the event listener for the ball, so we can tell when it gets lost.
            this._ball.listener = this;
        }

        /**
         * This gets invoked when our scene becomes the active scene; If the music is not muted and not
         * already playing, this starts it.
         *
         * @param previousScene the scene that used to be active.
         */
        activating (previousScene : nurdz.game.Scene) : void
        {
            // Let the super do its thing.
            super.activating (previousScene);

            // Set up what our font should be while this screen is active.
            this._renderer.context.font = '30px kenvector_futureregular';

            // Reset the game for another play.
            this.resetGame ();

            // Play the in-game music if we're not muted.
            if (musicMute == false)
                music[GameMusic.MUSIC_GAME].play ();
        }

        /**
         * This gets invoked when our scene is no longer going to be the active scene; stop our music from
         * playing.
         */
        deactivating (nextScene : nurdz.game.Scene) : void
        {
            // Let the super do it's thing
            super.deactivating (nextScene);

            // Stop our music if it's playing.
            if (music[GameMusic.MUSIC_GAME].isPlaying)
                music[GameMusic.MUSIC_GAME].pause ();
        }

        /**
         * Reset everything that tracks the game state back to the initial state
         */
        resetGame () : void
        {
            // Reset the score and lives.
            lives = 3;
            score = 0;

            // Set the initial score that an extra life will be earned at.
            this.nextLifeScore = EXTRA_LIFE_SCORE;

            // Reset the ball, make sure the paddle is visible, and then reset the brick grid.
            this.resetBall(this._ball);
            this._paddle.visible = true;
            this._brickGrid.reset (levelList[Utils.randomIntInRange (0, levelList.length - 1)]);

            // If the game somehow ended while an extra life was blinking, stop it now.
            this.blinkLifeIcon = false;
        }

        /**
         * Reset the ball, locking its position to the paddle and setting the lock offset so that the ball is
         * just touching the top edge of the paddle.
         *
         * This is used every time we want to reset the ball during the game (at game start or when a life is
         * lost) to allow the player a chance to control when and how the ball is released.
         */
        resetBall (ball : Ball) : void
        {
            // Reset the ball, locking it to the paddle.
            //
            // While the ball is locked to the paddle, its position is relative to the paddle. Adjust its
            // position so that it is above the center of the paddle.
            ball.reset(8, this._paddle);
            ball.position.setToXY(0, -ball.radius - (this._paddle.height / 2));
        }

        /**
         * This is triggered whenever the mouse is moved over the canvas.
         *
         * @param eventObj the event that represents the mouse movement.
         * @returns {boolean} true if we handled this event or false if not.
         */
        inputMouseMove (eventObj : MouseEvent) : boolean
        {
            // Get the current mouse position.
            this._mouse = this._stage.calculateMousePos (eventObj, this._mouse);

            // Constrain the mouse position to the allowable position for the paddle. This keeps the paddle
            // fully on the screen with a buffer of the radius of the ball on the left and the right, so that
            // even if the ball is attached to the paddle at the outermost extreme edge, it still cannot pass
            // off the edge of the screen.
            let newX = Utils.clampToRange(this._mouse.x,
                (this._paddle.width / 2),
                this._stage.width - (this._paddle.width / 2));
            this._paddle.jumpTo (newX);

            // We handled it.
            return true;
        }

        /**
         * This is triggered whenever the mouse is released after it was pressed over the canvas.
         *
         * @param  {MouseEvent} eventObj the event that represents the mouse release
         * @return {boolean} true if we handled the event or false if not
         */
        inputMouseUp (eventObj : MouseEvent) : boolean
        {
            // If the ball is parented to the paddle, release it now. This will allow it to move as it wants
            // to.
            if (this._ball.parent = this._paddle)
                this._ball.parent = null

            return true;
        }

        /**
         * Triggers every time a key is pressed
         * @param eventObj
         * @returns {boolean}
         */
        inputKeyDown(eventObj: KeyboardEvent): boolean
        {
            switch (eventObj.keyCode)
            {
                // For the F key, toggle between full screen mode and windowed mode.
                case KeyCodes.KEY_F:
                    this._stage.toggleFullscreen();
                    return true;

                // For the M key, toggle our mute state, playing or stopping music as needed
                case KeyCodes.KEY_M:
                    // Toggle the state and save the new value
                    musicMute = !musicMute;
                    saveMusicMute ();

                    // If we're now muted, pause the music, otherwise play it.
                    music[GameMusic.MUSIC_GAME].toggle ();
                    return true;
            }

            // Let the default happen
            return super.inputKeyDown(eventObj);
        }

        /**
         * Given a particular ball speed, return back the score multiplier that should apply for that particular
         * ball speed. The idea is that the faster the ball is moving, the higher the score multiplier.
         *
         * @param ballSpeed the incoming ball speed
         *
         * @return {number} the multiplier to use. This is always an integral value in the range of
         * MIN_SCORE_MULTIPLIER to MAX_SCORE_MULTIPLIER, inclusive.
         */
        private multiplier (ballSpeed : number) : number
        {
            // Clamp the ball speed so that it falls into the range of speeds that we consider for the
            // multiplier.
            ballSpeed = Utils.clampToRange (ballSpeed, MIN_MULTIPLIER_SPEED, MAX_MULTIPLIER_SPEED);

            // Now we need to convert the range to be in the range of 1 to 5 as a multiplier. This little bit
            // of math does a linear conversion between the minimum and maximum speeds to the range of the
            // minimum and maximum multipliers. We convert to an integer so that there are no fractional
            // multipliers.
            return Math.floor (((ballSpeed - MIN_MULTIPLIER_SPEED) / (MAX_MULTIPLIER_SPEED - MIN_MULTIPLIER_SPEED)) *
                (MAX_SCORE_MULTIPLIER - MIN_SCORE_MULTIPLIER) + MIN_SCORE_MULTIPLIER);
        }

        /**
         * Invoked once per game tick to update the frame and its contents.
         *
         * @param tick the current tick; increases by one for each invocation
         */
        update (tick : number) : void
        {
            // If we are blinking the life icon, update state for it.
            if (this._blinkLifeIcon)
            {
                // Decrement the duration; if it is done, turn the flag off. Otherwise, check and see if we
                // should toggle.
                this._blinkLifeDuration--;
                if (this._blinkLifeDuration <= 0)
                    this._blinkLifeIcon = false;
                else if (this._blinkLifeDuration % 7 == 0)
                    this._blinkOn = !this._blinkOn;
            }

            // Let the super update; this will shift the paddle to the new location and then allow the
            // ball to move. The ball will reset itself if it goes off the bottom or rebound off of the
            // walls if it can.
            super.update (tick);

            // Check to see if the ball is colliding with any bricks. This call will remove any bricks
            // collided with and return the number of such removed bricks back, so when it's more than 0 we
            // can updaTe the score.
            let brickPos = this._ball.checkBrickCollision(this._brickGrid);
            if (brickPos != null)
            {
                // We collided with a brick, so play the sound for that. This could also play a different
                // sound for different bricks, or potentially the sound could be attached to the brick. Or
                // both, and then a different sound plays altogether if the brick is not actually destroyed
                // yet.
                this._sounds[GameSoundItem.COLLIDE_BRICK].play (true);

                // Get the brick at this location and decrement its life. If it's dead, then it is time to
                // remove it and do scoring.
                let brick = this._brickGrid.getBrick (brickPos.x, brickPos.y);
                brick.life--;
                if (brick.life <= 0)
                {
                    this._brickGrid.removeBrick (brickPos.x, brickPos.y);

                    // The ball speed gets kicked up a bit whenever it hits a pink brick or when it impacts a
                    // brick in every 4th row.
                    if (brick.color == BrickSpriteColor.PINK || brickPos.y % 4 == 0)
                        this._ball.speed *= BALL_SPEED_INCREASE;

                    // Add some points for the brick. The base 100 points is affected by a multiplier based on the
                    // speed of the ball (so a longer volley means more points).
                    score += ((100 * brick.maxLife) * this.multiplier (this._ball.speed));

                    // Time to earn an extra life?
                    if (score >= this._nextLifeScore)
                    {
                        // We have earned an extra ball, so play the sound for that.
                        this._sounds[GameSoundItem.EXTRA_BALL].play ();

                        // Add a life and make sure the user knows.
                        lives++;
                        this.blinkLifeIcon = true;

                        // Update the score for the next free life. We use the set property here so that the text
                        // version and location will be updated too.
                        this.nextLifeScore += EXTRA_LIFE_SCORE;
                    }
                }
            }

            // Now we can check to see if the ball is colliding with the paddle.
            this._ball.checkPaddleCollision (this._paddle);
        }

        /**
         * Render the number of lives that are still left, as small paddle icons in the gutter below the
         * actual paddle.
         */
        renderLives () : void
        {
            // Calculate a value that is a bit bigger than the width of the actual image we use to display
            // lives; this will be the offset between each when we render.
            let imgWidth = Math.floor(this._lifeIcon.width * 1.5);

            // Calculate the position where the first life icon will be rendered.
            let blitX = this._stage.width - imgWidth;
            let blitY = this._stage.height - (this._lifeIcon.height * 2);

            // Now we can loop and render all of the lives.
            for (let i = 0; i < lives && i < MAX_LIVES_DISPLAYED ; i++ , blitX -= imgWidth)
            {
                // If this is the first life icon to render, AND we are blinking the life icon, AND it should
                // not be visible, then skip the render here.
                if (i == 0 && this._blinkLifeIcon == true && this._blinkOn == false)
                    continue;

                this._renderer.blit(this._lifeIcon, blitX, blitY);
            }
        }

        /**
         * This is called whenever our scene needs to be rendered.
         */
        render () : void
        {
            // Clear the screen with our background image.
            this._renderer.blit(this._backgroundImg, 0, 0);

            // Let the super render all of the entities now; this will display the ball, paddle and brick
            // grid.
            super.render ();

            // Now render lives and score. This makes sure that these items appear on top of everything
            // else, in case the ball is obstructing.
            this.renderLives();
            this._renderer.drawTxt("Score: " + score, 16, 32, 'white');
            this._renderer.drawTxt(this._nextLifeText, this._nextLifeTextPos, 32, 'red');

            // Render the current mute state now, on top of everything else.
            renderMuteState (this._stage);
        }

        /**
         * This gets invoked by the ball every time it goes off the bottom of the screen. We use this to
         * handle the ball being lost appropriately.
         *
         * @param {Ball} ball the ball that was lost.
         */
        ballLost (ball : Ball) : void
        {
            // Play the sound for a ball being lost.
            this._sounds[GameSoundItem.BALL_LOST].play ();

            // Make sure that the extra life icon stops blinking; we're about to use up the extra life.
            this.blinkLifeIcon = false;

            // Decrement the number of lives and check to see if the game is over or not.
            lives--;
            if (lives < 0)
            {
                // If the current score is higher than the highest high score, set and save the high score
                // into local storage (if possible) for the next time the page loads.
                if (score > highScore)
                {
                    highScore = score;
                    saveHighScore();
                }

                // Make sure that the paddle is marked as not being visible and then switch to the game over
                // scene and return. This makes sure that the paddle is not visible on the game over screen.
                //
                // The ball will be hidden because it should be marked as dead as part of the event that
                // invoked us to tell us it was lost.
                this._paddle.visible = false;
                this._stage.switchToScene('gameOver');
                return;
            }

            // Now we can reset the ball.
            this.resetBall(ball);
        }

        /**
         * This gets invoked by the ball whenever it collides with a paddle. This tells us which ball and
         * which paddle the collision happened between.
         *
         * Here we just need to play a sound because the ball handles it's reflection from the paddle on its
         * own.
         *
         * @param {Ball}   ball   the ball that collided with a paddle
         * @param {Paddle} paddle the paddle that the ball collided with
         */
        ballPaddle(ball: Ball, paddle: Paddle): void
        {
            this._sounds[GameSoundItem.COLLIDE_PADDLE].play ();
        }

        /**
         * This event triggers when a ball collides with a wall, causing it to be reflected back
         *
         * @param {Ball} ball the ball that collides with the wall
         */
        ballWall (ball: Ball) : void
        {
            this._sounds[GameSoundItem.COLLIDE_WALL].play ();
        }

        /**
         * Invoked when the image we want to use for our background is loaded; this sets up our member
         * variable for the background.
         *
         * When the image provided is smaller than the stage, the image is tiled to a size that will fill the
         * stage first.
         *
         * @param image The image that was loaded
         */
        private setBackgroundImg = (image : HTMLImageElement): void =>
        {
            // Create a tiles version of the coming image.
            this._backgroundImg = tileImage(this._stage, image);
        }
    }
}
