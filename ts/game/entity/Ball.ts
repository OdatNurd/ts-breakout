module nurdz.game
{
    /**
     * An interface that classes can conform to in order to be told when various ball events happen during the
     * game.
     */
    export interface BallEventListener
    {
        /**
         * This event triggers when a ball is "lost" by falling off the bottom of the screen. When this is
         * detected, the ball stops updating until it is reset.
         *
         * @param {Ball} ball the ball that was lost
         */
        ballLost(ball: Ball): void;

        /**
         * This event triggers when a ball collides with a wall, causing it to be reflected back
         *
         * @param {Ball} ball the ball that collides with the wall
         */
        ballWall (ball: Ball) : void;

        /**
         * This event triggers when a ball collides with a paddle, causing it to be reflected back.
         *
         * @param {Ball}   ball   the ball that collided with a paddle
         * @param {Paddle} paddle the paddle that the ball collided with
         */
        ballPaddle(ball: Ball, paddle: Paddle): void;
    }

    /**
     * When we impact with the paddle, our X speed is modified based on how far away from the center of
     * the paddle we are (the farther, the faster). This value is used to damp that value down so that the
     * ball doesn't go too crazy at the outer bounds of the paddle.
     *
     * @type {number}
     */
    export const BALL_PADDLE_SPEED_DAMPER = 0.35;

    /**
     * This is used to provide a human readable sprite number to the different colors of ball sprites that
     * exist in the ball sprite sheet.
     */
    enum BallSpriteColor
    {
        WHITE,
        BLACK,
        BLUE,
        YELLOW,

        SPRITE_COUNT
    };

    /**
     * This entity represents the ball in the game.
     */
    export class Ball extends Entity
    {
        /**
         * The velocity of the ball in the X and Y dimensions
         */
        private _velocity : Vector2D;

        /**
         * The speed of the ball. This is used to specify the magnitude of the _velocity vector, so that when
         * one of the components changes we can ensure that the direction remains but the speed remains
         * more constant.
         */
        private _speed: number;

        /**
         * The object that gets told when various events occur. If this is null, no events are triggered. Only
         * one object can be an event listener at a time.
         *
         * @type {BallEventListener}
         */
        private _listener: BallEventListener;

        /**
         * An indication of whether this ball is "live" or not; a live ball updates and renders itself to the
         * screen, while a dead ball does not.
         *
         * The ball becomes dead when it goes off the bottom of the screen and doesn't become live again until
         * it is reset.
         *
         * @type {boolean}
         */
        private _dead: boolean;

        /**
         * The entity that the ball is parented to, if any. While the ball has a parent, its update method
         * does not do anything motion related (including collision detection).
         *
         * Also, while there is a parent set, the position property of the entity is interpreted as being
         * relative to the parent entity instead of being an absolute screen position. The code that sets
         * and resets the parent takes care of modifying the position property so that the current position of
         * the ball on the screen is captured.
         *
         * @type {Entity}
         */
        private _parent: Entity = null;

        /**
         * Set the object that will get told of events for this ball when they occur.
         *
         * @param {BallEventListener} newListener the new listener
         */
        set listener(newListener: BallEventListener) {
            this._listener = newListener;
        }

        /**
         * Get the object that will get told of events for this ball when they occur.
         *
         * @return {BallEventListener} the current ball event listener for this ball, or null if there is
         * none.
         */
        get listener(): BallEventListener {
            return this._listener;
        }

        /**
         * Get the entity that is currently set to be our parent, or null if there is not a parent set.
         *
         * While there is a parent set, the ball does not do any updates regarding position or collision
         * detection, and its position is relative to this parent entity.
         *
         * @return {Entity} the entity that we're currently parented to, or null if there isn't one.
         */
        get parent(): Entity {
            return this._parent;
        }

        /**
         * Set the entity that we are currently parented to, which can be null to break any current
         * child/parent relationship. When we have a parent, our position becomes relative to the parent
         * instead of to the stage.
         *
         * This takes care of adjusting the current position so that it becomes relative to the new parent
         * or the stage as appropriate, so that the position of the ball remains consistent regardless.
         *
         * @param {Entity} newParent the entity to make our parent or null to remove an existing parent
         */
        set parent(newParent: Entity) {
            // If we are currently locked to a parent, then adjust our position by its position so that we put
            // ourselves back into an absolute world position based on where the parent currently is.
            if (this._parent != null)
                this._position.translate(this._parent.position);

            // Set or clear the new parent.
            this._parent = newParent;

            // If we have a new parent now, then we need to adjust our position so that it becomes relative to
            // the position of the new parent.
            if (newParent != null) {
                this._position.x -= this._parent.position.x;
                this._position.y -= this._parent.position.y;
            }
        }

        /**
         * Get the speed that the ball is traveling in the direction that it is currently pointing. This is
         * expressed as the magnitude of a velocity vector
         *
         * @return {number} the current speed of the ball
         */
        get speed () : number {
            return this._speed;
        }

        /**
         * Set the speed that the ball is traveling in the direction that it is currently pointing. This is
         * expressed as the magnitude of a velocity vector.
         *
         * @param {number} newSpeed the new speed for the ball to travel in
         */
        set speed (newSpeed : number) {
            // Save the speed and then adjust the velocity vector to have the correct length.
            this._speed = newSpeed;
            this._velocity.magnitude = newSpeed;
        }

        /**
         * Construct a new ball that will render on the stage provided.
         *
         * @param stage the stage the ball will be on
         */
        constructor (stage : Stage)
        {
            // Invoke the super. Note that we don't provide any location or dimensions here. These will
            // get set later.
            super ("ball", stage, 0, 0, 0, 0, 1, {}, {}, 'red');

            // Set up the sprite sheet that contain our ball images. THe size of the entity is determined by
            // the size of the sprites, so we let the callback handle that.
            this._sheet = new SpriteSheet(stage, "ball_4_1.png", BallSpriteColor.SPRITE_COUNT, 1, true, this.setDimensions);
            this._sprite = BallSpriteColor.YELLOW;

            // Set up a default velocity vector and a speed.
            this._velocity = new Vector2D (1, 0);
            this._speed = 1;

            // Initially, there is no listener.
            this._listener = null;

            // Now we can reset the ball.
            this.reset (1);
        }

        /**
         * Reset the ball.
         *
         * If an entity is provided, the ball will parent itself to that entity and set its speed to travel upwards.
         * Otherwise, it sets its position to the center of the stage and travels downwards.
         */
        reset (newSpeed : number, entity : Entity = null) : void
        {
            if (entity)
            {
                // Set our parent to the entity provided and move upwards. We use the property setter here
                // because it does magic for us.
                this.parent = entity;
                this._velocity.directionDeg = Utils.randomIntInRange (210, 320);
            }
            else
            {
                // Reset to the center of the screen and moving downward.
                this._position.setToXY (stage.width / 2, stage.height / 2);
                this._velocity.directionDeg = Utils.randomIntInRange (30, 150);
            }

            // Now set the speed to the one passed in and become alive.
            this.speed = newSpeed;
            this._dead = false;
        }

        /**
         * Given an entity that supports the brick grid interface, check to see if the ball is colliding with
         * any of the bricks in the grid or not. The ball can only collide with a single brick on every
         * pass through this function.
         *
         * The return value is null if the ball did not collide with anything or a point which represents
         * the spot (in brick grid coordinates) that a brick was collided with.
         *
         * When this reports a collision, the trajectory of the ball is modified but the grid is left alone
         * so that the caller can take special action; it should probably at least remove the brick.
         *
         * @param brickGrid  The object that represents the brick grid to collide with
         * @returns {Point} the point (in brick grid space) that a brick was collided with, or null if none
         */
        checkBrickCollision (brickGrid : BrickGrid) : Point
        {
            // Convert the ball Y position by shifting it upwards by the offset that the bricks are offset
            // from the top of the screen, so that we can properly convert the ball coordinates.
            let ballY = this._position.y - brickGrid.position.y;

            // Convert our ball location (which is in screen coordinates) into "brick coordinates" to see
            // where on the grid we currently are.
            let bCol = Math.floor (this._position.x / brickGrid.brickWidth);
            let bRow = Math.floor (ballY / brickGrid.brickHeight);

            // If the current position of the ball is outside of the brick grid, we can return without
            // doing anything further.
            if (bRow < 0 || bCol < 0 || bRow >= brickGrid.gridRows || bCol >= brickGrid.gridColumns)
                return null;

            // If there is no brick at this position, there is nothing to do.
            if (brickGrid.visibleBrickAtColRow (bCol, bRow) == false)
                return null;

            // Now calculate the grid position for where we started the frame so that we can determine how far
            // we might have moved.
            let bPrevCol = Math.floor ((this._position.x - this._velocity.x) / brickGrid.brickWidth);
            let bPrevRow = Math.floor ((ballY - this._velocity.y) / brickGrid.brickHeight);

            let bothTestsFailed = true;

            // If the ball column between now and the previous frame changed, and there is not a brick in
            // the column that we came from, then we should reflect horizontally.
            if (bCol != bPrevCol && brickGrid.visibleBrickAtColRow (bPrevCol, bRow) == false)
            {
                this._velocity.flipX ();
                bothTestsFailed = false;
            }

            // If the ball row between now and the previous framer changed, and there is not a brick in
            // the row that we came from, then we should reflect vertically.
            if (bRow != bPrevRow && brickGrid.visibleBrickAtColRow (bCol, bPrevRow) == false)
            {
                this._velocity.flipY ();
                bothTestsFailed = false;
            }

            // Each test above checks to see if a bounce in a direction is blocked, and only reflects if
            // it is not. However, in the case where neither of those tests trigger, it means that the
            // ball intersected in a way where it jumped both column and row (i.e. through the diagonal
            // corner without touching a brick on either side).
            //
            // In this case, we want to reflect in both directions and go back where we came from.
            if (bothTestsFailed)
                this._velocity.reverse ();

            // Return the collision spot.
            return new Point (bCol, bRow);
        }

        /**
         * Check to see if the current position of the ball is colliding with the provided paddle or not.
         * This assumes that the call is made AFTER the ball has been moved along its speed vector
         *
         * When a collision is detected, the velocity of the ball is adjusted appropriately. Additionally,
         * our position may also be modified so that we don't visually interfere with the paddle.
         * @param paddle
         */
        checkPaddleCollision (paddle : Paddle) : void
        {
            // TODO Maybe this is not safe in the bottom corners?
            //
            // What exactly happens if the update() method detects that the ball is rebounding off of the
            // wall down by the paddle, and then we get called, collide with the paddle, and decide to
            // back up? Probably nothing good.

            // If we are moving downward and the line between where we used to be and where we are now
            // intersects with the paddle, consider that we touched it.
            if (this._velocity.y > 0 &&
                paddle.intersectWithSegmentXY (this._position.x - this._velocity.x,
                                               this._position.y - this._velocity.y,
                                               this._position.x,
                                               this._position.y))
            {
                // If there is a listener, tell it we're colliding with this paddle.
                if (this._listener != null)
                    this._listener.ballPaddle(this, paddle);

                // Unlike a reflection of the ball off of the sides or a brick, which just changes the
                // direction of one of the vector components, for the paddle we change the X component of the
                // vector and and just flip the Y. This allows for some control over the direction the ball
                // goes.
                this._velocity.x = (this._position.x - paddle.position.x) * BALL_PADDLE_SPEED_DAMPER;
                this._velocity.flipY ();

                // Changing one of the vector components changes the magnitude, so set it back to what it
                // should be. The only things that should change the velocity of the ball is the game telling
                // us to kick our speed up (which does this inherently).
                this._velocity.magnitude = this._speed;
            }
        }

        /**
         * This is called every frame update (tick tells us how many times this has happened) to allow us
         * to update our position.
         *
         * @param stage the stage that we are on
         * @param tick the current engine tick; this advances one for each frame update
         */
        update (stage : Stage, tick : number) : void
        {
            // Don't update if we're dead.
            if (this._dead)
                return;

            // If we are locked to an entity, don't update ourselves; we shouldn't move in this case.
            if (this._parent != null)
                return;

            // We want to update, which we do by moving the ball along its speed vector. First we want to
            // check  to see if moving in this manner would cause the ball to be closer to the edge of the
            // screen than its radius (due to its position being determined by its center).
            let newXPos = this._position.x + this._velocity.x;
            if (newXPos <= this.radius || newXPos >= stage.width - this.radius)
            {
                // If we moved the ball using our regular speed vector, it would move us too close to the left
                // or right edge of the screen. In this case we want our movement to be such that we stop
                // right when our position puts our edge on the edge of the screen. To do that we determine just
                // how much we have to move on the X axis in our current direction in order to do that. This
                // might be 0 if the ball is currently exactly on the bounds we desire.
                let axialDisplacement = 0;
                if (newXPos < this._position.x)
                    axialDisplacement = this._position.x - this.radius;
                else if (newXPos > this._position.x)
                    axialDisplacement = (stage.width - this.radius) - this._position.x;

                // If we are actually supposed to move, do it; if we happened to start in a position where we
                // were already exactly where we wanted to be, we don't need to do anything.
                if (axialDisplacement != 0)
                {
                    // Make a copy of our speed vector and alter its magnitude so that following it will make
                    // us move exactly as far as we want on the X axis and the proper amount on the Y axis.
                    let shortSpeed = this._velocity.copy();
                    shortSpeed.magnitude = axialDisplacement / Math.cos(shortSpeed.direction);

                    // Now we can translate based on our shorted speed vector.
                    this._position.translate(shortSpeed);
                }
            }
            else
                // We can just do a regular translate of the point, this isn't going to put anything off of
                // the screen
                this._position.translateXY (this._velocity.x, this._velocity.y);

            // If our Y position shows that we're about to go off the bottom of the screen, make ourselves
            // dead, trigger the event (if we can) that tells this, and leave.
            if (this._position.y >= this._stage.height - this.radius)
            {
                this._dead = true;
                if (this._listener != null)
                    this._listener.ballLost(this);
                return;
            }

            // Check to see if we should bounce off of the ceiling of the screen. This is adjusted by our
            // current radius, so we don't visually appear to ever go off of the screen on the top.
            if (this._position.y <= this.radius && this._velocity.y < 0)
            {
                // If there is a current listener, tell it we're colliding with the wall.
                if (this._listener != null)
                    this._listener.ballWall(this);

                // Make a reverse copy of our speed vector and then set its magnitude to how far back we
                // have to go along it to get our center point to be exactly our radius away from the top
                // edge of the screen.
                let revSpeed = this._velocity.copyReversed ();
                revSpeed.magnitude = (this.radius - this._position.y) / Math.sin (revSpeed.direction);

                // Translate backwards along this new vector to reposition ourselves and then reverse our
                // Y velocity.
                this._position.translate (revSpeed);
                this._velocity.flipY ();
            }

            // Similar to what we did for the ceiling, check if we're colliding with the left or right of
            // the stage. This only triggers if we're colliding with the side that we're moving towards.
            if ((this._position.x >= stage.width - this.radius && this._velocity.x > 0) ||
                (this._position.x <= this.radius && this._velocity.x < 0))
            {
                // If there is a current listener, tell it we're colliding with the wall.
                if (this._listener != null)
                    this._listener.ballWall(this);

                // Here we just will flip the X speed. We know from the pre-movement code above that we never
                // get too close to the screen bounds in the general case, so there's nothing special to do
                // here.
                this._velocity.flipX ();
            }
        }

        /**
         * Render the ball at the provided location and using the given renderer.
         *
         * We assume our position is at our center.
         *
         * @param x the x of the center of the ball
         * @param y the y of the center of the ball
         * @param renderer the renderer to blit with
         */
        render (x : number, y : number, renderer : Renderer) : void
        {
            // Don't render if we're dead.
            if (this._dead)
                return;

            // The position we get invoked with represents our current position, but when we are locked to
            // another entity, our position is actually an offset from that entity. As a result, in that case
            // we adjust the position given so we still render in the correct location.
            if (this._parent)
            {
                x += this._parent.position.x;
                y += this._parent.position.y;
            }

            // Now invoke the super to actually render.
            super.render (x, y, renderer);
        }

        /**
         * Set the dimensions of the ball sprite based on the sprite sheet provided
         *
         * @param sheet the sprite sheet that was loaded
         */
        private setDimensions = (sheet : SpriteSheet) : void =>
        {
            // Set our dimensions to be circular and based on the dimensions of the sprites in the sheet.
            this.makeCircle(sheet.width / 2, true);
        }
    }
}
