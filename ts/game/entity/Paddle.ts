module nurdz.game
{
    /**
     * The distance from the edge of the screen that the paddles is rendered. Larger values mean farther from
     * the edge of the screen and thus less reaction time.
     *
     * @type {number}
     */
    export const PADDLE_EDGESPACE = 60;

    /**
     * This is used to provide a human readable sprite number to the different colors of paddle sprites that
     * exist in the paddle sprite sheet.
     */
    enum PaddleSpriteColor {
        RED,
        WHITE,
        BLUE,

        SPRITE_COUNT
    };

    /**
     * The paddle entity is the entity that controls the paddle used by the player(s). Also included is an
     * AI module that will allow it to attempt to track the ball in order to return a serve.
     */
    export class Paddle extends Entity
    {
        /**
         * When this is not -1, it's the position that our X position should be set to the next time that
         * we update. This ensures that the paddle is moved at a set time, since input events can occur at
         * any time and we want to rigidly control our update loop.
         */
        private _newPos : number = -1;

        /**
         * An indication of whether or not we render ourselves when asked; this is generally only false
         * while the game over screen is being displayed.
         */
        private _visible : boolean;

        /**
         * Get the current visibiilty state of the paddle; this tells you if it is rendering itself when asked
         * or not.
         *
         * @return {boolean} true if the paddle will render itself when asked, or false otherwise.
         */
        get visible () : boolean { return this._visible; }

        /**
         * Change the current visibility state of the paddle.
         *
         * @param newVisible true to render when asked, or false to remain hidden
         */
        set visible (newVisible: boolean) { this._visible = newVisible; }

        /**
         * Construct a new paddle handled by the provided stage. The paddle is always initially horizontally
         * centered on the stage.
         *
         * @param stage the stage to render on
         */
        constructor (stage : Stage)
        {
            // Invoke the super to set ourselves up. We reference our bounds with our origin at the center
            // of ourselves. We always start with our position centered vertically, but we need to be told
            // our horizontal position.
            super ("paddle", stage, stage.width / 2, stage.height - PADDLE_EDGESPACE, 0, 0, 1, {}, {}, 'red');

            // Set up our sprite sheet; when this loads, our dimensions will be set to be the size of the
            // sprites in the sprite sheet.
            this._sheet = new SpriteSheet(stage, "paddle_1_3.png", 1, PaddleSpriteColor.SPRITE_COUNT, true, this.setDimensions);
            this._sprite = PaddleSpriteColor.BLUE;

            // We start visible.
            this._visible = true;

            // Set up our dimensions.
            this._width = 100;
            this._height = 10;

            // We want our origin to be at our center.
            this._origin.setToXY (this._width / 2, this._height / 2);
        }

        /**
         * This is invoked every frame to update this paddle.
         *
         * @param stage the stage the paddle is on
         * @param tick the game tick, for timing purposes
         */
        update (stage : Stage, tick : number) : void
        {
            // If we have a new position, then set it now and remove the flag.
            if (this._newPos != -1)
            {
                this._position.x = this._newPos;
                this._newPos = -1;
            }
        }

        /**
         * Render the paddle at the provided location and using the given renderer.
         *
         * @param x the x of the paddle
         * @param y the y of the paddle
         * @param renderer the renderer to blit with
         */
        render (x : number, y : number, renderer : Renderer) : void
        {
            // Leave if we're not supposed to be visible.
            if (this._visible == false)
                return;

            // Now invoke the super to actually render.
            super.render (x, y, renderer);
        }

        /**
         * Jump this paddle directly so that its horizontal position is the position passed in. This does
         * limit checking to ensure the position is set to be as valid as possible.
         *
         * @param newPos the new X position.
         */
        jumpTo (newPos : number) : void
        {
            // Set up what our new position should be in the next update, and make sure that it's clamped
            // to an acceptable range.
            this._newPos = Utils.clampToRange (newPos, 0, this._stage.width - 1);
        }

        /**
         * Set the dimensions of the ball sprite based on the sprite sheet provided
         *
         * @param sheet the sprite sheet that was loaded
         */
        private setDimensions = (sheet: SpriteSheet): void =>
        {
            // Set up our dimensions based on the size of the sprites in this sprite sheet, and make sure
            // that our origin is at our center.
            this._width = sheet.width;
            this._height = sheet.height;
            this._origin.setToXY(this._width / 2, this._height / 2);
        }
    }
}
