module nurdz.game
{
    /**
     * This is used to provide a human readable sprite number to the different colors of brick sprites that
     * exist in the brick sprite sheet.
     */
    export enum BrickSpriteColor
    {
        // There is no brick; this spot is empty and does not render/block the ball.
        NONE=-1,

        // The sprite colors, in order
        BLACK,
        WHITE,
        RED,
        ORANGE,
        YELLOW,
        GREEN,
        BLUE,
        PINK,

        SPRITE_COUNT
    };

    /**
     * This entity represents the ball in the game.
     */
    export class Brick extends Entity
    {
        /**
         * The maximum amount of life that this brick has; if this is a positive number, it represents the
         * number of hits that this brick needs before it will be removed. In this case after the first
         * collision with the ball, the ball will begin to render itself as "damaged".
         *
         * When this value is negative, this brick is invincible; no number of hits will cause the brick to
         * be destroyed. It will also never appear damaged.
         */
        private _maxLife : number;

        /**
         * How much "life" this brick has. Every time the ball collides with
         * this brick, this number decrements, and the brick is removed when
         * the life is 0.
         */
        private _life : number;

        /**
         * A sprite sheet that contains some sprite overlays for bricks that are considered to be "damaged".
         * This occurs when their life value is not at its maximum value, indicating that this brick needs
         * more hits to be destroyed.
         *
         * These sprites are the same size as the brick sprites and are mostly transparent, so that they can
         * be rendered directly on top of the bricks as needed.
         */
        private _crackSheet : SpriteSheet;

        /**
         * This represents the sprite in the _crackSheet sprite sheet that this brick should use to render its
         * damage overlay.
         *
         * This is < 0 when this sprite is not currently damaged (and so no such overlay should be rendered).
         *
         * @type {number}
         */
        private _crackSprite : number;

        /**
         * This indicates if this brick is currently invincible or not. When this is set to true, changes
         * to the life of the brick do nothing. Most bricks are not invincible by default.
         *
         * @type {boolean}
         */
        private _invincible : boolean;

        /**
         * Set the color that this brick renders with
         *
         * @param {BrickSpriteColor} newColor the new color
         */
        set color (newColor : BrickSpriteColor) { this._sprite = newColor; }

        /**
         * Get the color that this brick renders with.
         *
         * @return {BrickSpriteColor} the current brick color
         */
        get color () : BrickSpriteColor { return this._sprite; }

        /**
         * Change the current invincible state for this brick; when it is invincible, collisions with the
         * ball do not have any effect on it.
         *
         * @param {boolean} newStatus true if this brick should be invincible, or false otherwise.
         */
        set isInvincible (newStatus : boolean) { this._invincible = newStatus; }

        /**
         * Get the current invincibiliy state for this brick; when it is invincible, collisions with the ball
         * do not have any effect on it.
         *
         * @return {boolean} true if this brick is currently invincible, or false otherwise.
         */
        get isInvincible () : boolean { return this._invincible; }

        /**
         * Set the maximum amount of life that this brick should have. This is used along with the life value
         * to determine if this brick should appear damaged or not.
         *
         * @param {number} newMax the new maximum life value
         */
        set maxLife (newMax : number) { this._maxLife = newMax; }

        /**
         * Get the maximum amount of life that this brick should have. This is the amount of life that the
         * brick started with, which might be different than the current life level.
         *
         * @return {number} the maximum life value for this brick
         */
        get maxLife () : number { return this._maxLife; }

        /**
         * Set the life that this brick should have; this is an indication of how many impacts with the ball
         * the brick needs before it is actually removed from the grid.
         *
         * As long as the currently set life value is smaller than the maximum life value currently set, this
         * brick will render itself as damaged.
         *
         * @param {number} newLife the new life value
         */
        set life (newLife : number)
        {
            // The life value can't change if this brick is invincible
            if (this._invincible)
                return;

            // Set the new life value now.
            this._life = newLife;

            // The brick is damaged now. If it was not before, select a random starting crack sprite for it.
            // Otherwise, skip to the next sprite so that there is a visual indication that something happend.
            if (this._life < this._maxLife)
            {
                if (this._crackSprite < 0)
                    this._crackSprite = Utils.randomIntInRange (0, this._crackSheet.count - 1);
                else
                    this._crackSprite = (this._crackSprite + 1) % this._crackSheet.count;
            }

            // The brick was damaged but is not any longer; remove the crack sprite.
            else
                this._crackSprite = -1;
        }

        /**
         * Get the current life of this brick; this is the number of hits needed on this brick before it will
         * be removed from the grid. This value may be different from the maximum life of the brick.
         *
         * @return {number} the current life of this brick; 0 if the brick is currently gone
         */
        get life () : number { return this._life; }

        /**
         * Construct a new ball that will render on the stage provided.
         *
         * @param stage the stage the ball will be on
         */
        constructor (stage : Stage)
        {
            // Invoke the super. Note that we don't provide any location or dimensions here. These will
            // get set later.
            super ("brick", stage, 0, 0, 0, 0, 1, {}, {}, 'red');

            // Load our sprite sheet
            this._sheet = new SpriteSheet(stage, "bricks_1_8.png", 1, BrickSpriteColor.SPRITE_COUNT, true);
            this._crackSheet = new SpriteSheet(stage, "brick_cracks_1_2.png", 1, 2, true);

            // Reset intenal state.
            this.reset ();
        }

        /**
         * Reset this brick back to its default values. This sets the brick to be invisible, with a single
         * life point and no visible damage.
         */
        reset ()
        {
            // Sprite is now empty with a single life point and no visible damage. It is also not invincible.
            this._sprite = BrickSpriteColor.NONE;
            this._invincible = false;
            this._crackSprite = -1;
            this._maxLife = 1;
            this._life = 1;
        }

        /**
         * Render the brick at the provided location and using the given renderer.
         *
         * @param x the x co-ordinate of the brick
         * @param y the y co-ordinate of the brick
         * @param renderer the renderer to blit with
         */
        render (x : number, y : number, renderer : Renderer) : void
        {
            // Leave if we have no sprite assigned; we should not render.
            if (this._sprite == BrickSpriteColor.NONE)
                return;

            // Use the super to do our render for us; it will use our sprite to render as we see fit. Then
            // render our crack sprite, if we are currently damaged.
            super.render (x, y, renderer);
            if (this._crackSprite >= 0)
                renderer.blitSprite (this._crackSheet, this._crackSprite, x, y);
        }
    }
}
