module nurdz.game
{
    /**
     * The blinking text entity is an entity that is used to display some blinking text. This allows changing
     * the text displayed, the font used, and the position. The position is always assumed to be the center
     * of the dimensions.
     */
    export class BlinkingText extends Entity
    {
        /**
         * The text that we will be displaying
         */
        private _text: string;

        /**
         * The font specification that we use to display.
         */
        private _font: string;

        /**
         * The list of colors used to blink our text.
         *
         * @type {Array<string>}
         */
        private _blinkColors: Array<string> = ['#ffffff', '#aaaaaa'];

        /**
         * The current color in _blinkColors that is being used to render blinking text.
         *
         * @type {number}
         */
        private _blinkIndex: number = 0;

        /**
         * The number of game ticks that happen before the color changes to the next one in the color list.
         * The higher the number, the slower the blink speed.
         *
         * @type {number}
         */
        private _blinkTicks: number = 7;

        /**
         * Construct a new blinking text entity to be handled by the provided stage.
         *
         * @param stage the stage to render on
         * @param font the font to display the text in
         * @param text the text to display
         */
        constructor(stage: Stage, font: string, text : string)
        {
            // Invoke the super to set ourselves up. We reference our bounds with our origin at the center
            // of ourselves. We always start with our position centered vertically, but we need to be told
            // our horizontal position.
            super("text", stage, stage.width / 2, stage.height, 0, 0, 1, {}, {}, 'red');

            // Set the font (raw) and then the text. We set the font raw so that we don't do a duplicate font
            // measurement.
            this._font = font;
            this.text = text;
        }

        /**
         * Obtain the text that this is currently set to display.
         *
         * @return {string} the currently set text
         */
        get text(): string
        {
            return this._text;
        }

        /**
         * Change the text that is to be displayed by this entity. This will cause the extents of the entity
         * to be re-calculated.
         */
        set text(newText: string)
        {
            this._text = newText;
            this.resetDimensions();
        }

        /**
         * Obtain the font that will be used to display this text.
         */
        get font () : string
        {
            return this._font;
        }

        /**
         * Change the font that is to be used to display this entity. This will cause the extents of the
         * entity to be re-calculated
         */
        set font (newFont : string)
        {
            this._font = newFont;
            this.resetDimensions();
        }

        /**
         * Change the set of colors that are used to blink this text. This inherently resets the blink
         * sequence.
         */
        set colors (newColors: Array<string>)
        {
            this._blinkColors = newColors;
            this._blinkIndex = 0;
        }

        /**
         * Get the current set of colors that are used to blink this text.
         */
        get colors () : Array<string>
        {
            return this._blinkColors;
        }

        /**
         * Get the number of ticks between color changes, which controls the speed of the blink. This counts
         * ticks as frames, so a value of 30 means the color changes once per second.
         */
        get blinkTicks () : number
        {
            return this._blinkTicks;
        }

        /**
         * Set the number of ticks between color changes, which controls the speed of the blink. This counts
         * ticks as frames, so a value of 30 means the color changes once per second.
         */
        set blinkTicks (newTicks : number)
        {
            this._blinkTicks = newTicks;
        }

        /**
         * Using the current text and font, determine exactly how wide and tall the current entity should be,
         * and reset the origin to be the center.
         */
        private resetDimensions () : void
        {
            // Get the renderer from the current stage, and from that the canvas context.
            let context = (<CanvasRenderer> this._stage.renderer).context;

            // Now we need to save the state of the context, swap the font for our font, and measure the
            // text, then reset the context.
            context.save();
            context.font = this._font;
            this._width = context.measureText(this._text).width;
            context.restore();

            // Our height comes from our font, which should start with the font size in pixels for this to
            // work.
            this._height = parseInt(this._font, 10);

            // We want our origin to be at our center.
            this._origin.setToXY(this._width / 2, this._height / 2);
        }

        /**
         * This is invoked every frame to update this text.
         *
         * @param stage the stage the paddle is on
         * @param tick the game tick, for timing purposes
         */
        update(stage: Stage, tick: number): void
        {
            // Every 7 ticks (roughly every quarter second) change the color of the text
            if (tick % this._blinkTicks == 0)
                this._blinkIndex = (this._blinkIndex + 1) % this._blinkColors.length;
        }

        /**
         * Render the text using the renderer provided. The position provided represents the actual position
         * of the text as realized on the screen, which may be different from its actual position if scrolling
         * or a view port of some sort is in use.
         *
         * @param x the x location to render the text at, in stage coordinates (NOT world)
         * @param y the y location to render the text at, in stage coordinates (NOT world)
         * @param renderer the class to use to render the text
         */
        render(x: number, y: number, renderer: CanvasRenderer): void
        {
            // Translate the context so the origin is at the point we were given; this also saves the state of
            // the context. We don't need to take the entity origin into account here because we're centering
            // around the position provided below no matter what.
            renderer.translateAndRotate(x, y);

            // Alter the font and text alignment so that text is centered horizontally and vertically around
            // the given position.
            renderer.context.font = this._font;
            renderer.context.textAlign = "center";
            renderer.context.textBaseline = "middle";

            // Now render the text with the current color and then restore the context.
            renderer.drawTxt(this._text, 0, 0, this._blinkColors[this._blinkIndex] );
            renderer.restore();
        }
    }
}
