module nurdz.game
{
    /**
     * How many columns of bricks the play area contains.
     *
     * @type {number}
     */
    const BRICK_COLS = 10;

    /**
     * How many rows of bricks the play area contains.
     *
     * @type {number}
     */
    const BRICK_ROWS = 16;

    /**
     * How many pixels the bricks render from the top of the screen; this makes room for things like the score
     * to be displayed above the grid.
     *
     * @type {number}
     */
    const BRICK_RENDER_OFFSET = 50;

    /**
     * This type specifies the conversion of a string constant of a fixed length to one of the brick colors.
     * Here the characters are an upper case version of the first letter of each color, except for Black and
     * White, which are "1" and "0" respectively, partially because of their binary nature and partially
     * because blue and black start with the same letter.
     *
     * This is used as a part of level definitions, to map a color string to a numeric color in a way that is
     * a little nicer to look at (and always a single character, so the tables line up nicer).
     */
    export type BrickColorString = " " | "1" | "0" | "R" | "O" | "Y" | "G" | "B" | "P";

    /**
     * This type specifies that a row of bricks is an array of color strings. This is used in level
     * definitions to easily specify a row of bricks in a level layout.
     *
     * Unfortunately there does not seem to be any way to create such a type definition in a way that
     * indicates that there be an exact number of elements in the array, so we need to do that at runtime.
     */
    export type BrickRow = Array<BrickColorString>;

    /**
     * This type specifies that an entire brick layout is an array of brick rows. This is used as the type for
     * a level definition.
     *
     * As mentioned in BrickRow, this needs to be validated at runtime because there does not seem to be any
     * way to indicate that an array needs a set number of elements in a type declaration.
     */
    export type BrickLayout = Array<BrickRow>;

    /**
     * This entity represents the grid of bricks in the game.
     */
    export class BrickGrid extends Entity
    {
        /**
         * The bricks that exist on the screen. The values in the array are instances of the Brick entity
         * which will represent each brick that is (or is not) there.
         */
        private _bricks: Array<Brick>;

        /**
         * The number of bricks that are still left to destroy in the _bricks array.
         */
        private _bricksLeft : number;

        /**
         * The sprite sheet that contains the images of the bricks in the game
         */
        private _brickSheet: SpriteSheet;

        /**
         * The number of rows of bricks that can appear (at most) in the brick grid.
         */
        get gridRows () : number { return BRICK_ROWS; }

        /**
         * The number of columns of bricks that can appear (at most) in the brick grid.
         */
        get gridColumns () : number { return BRICK_COLS; }

        /**
         * The number of bricks remaining in the grid; when this is 0, the grid is completely empty.
         */
        get bricksLeft () : number { return this._bricksLeft; }

        /**
         * How wide each brick is, in pixels.
         */
        get brickWidth () : number {return this._brickSheet.width; }

        /**
         * How tall each brick is, in pixels.
         */
        get brickHeight () : number { return this._brickSheet.height; }

        /**
         * Construct a new brick grid that will render on the stage provided.
         *
         * @param stage the stage the brick grid will be on
         */
        constructor (stage : Stage)
        {
            // Invoke the super. We don't have a set size but our position is set to assume that the top is
            // where the predefined render offset is located.
            //
            // Sometime in the future this might want to set up the width and height based on knowing the
            // brick size and the grid size; for right now, we assume that the predefined grid size works with
            // the brick size in order to exactly fill the desired area.
            super ("brickGrid", stage, 0, BRICK_RENDER_OFFSET, 0, 0, 1, {}, {}, 'red');

            // Load the sprite sheet for the brick images.
            this._brickSheet = new SpriteSheet(stage, "bricks_1_8.png", 1, BrickSpriteColor.SPRITE_COUNT, true);

            // Initialize our underlying array of bricks. This just needs to be some empty brick instances;
            // they will get their properties later.
            this._bricks = new Array (BRICK_COLS * BRICK_ROWS);
            for (let i = 0 ; i < BRICK_COLS * BRICK_ROWS ; i++)
                this._bricks[i] = new Brick (stage);
            this._bricksLeft = 0;
        }

        /**
         * Given a brick color string, return back the sprite color of the brick that associates with that
         * particular specification/
         *
         * @param  {BrickColorString} spec the color specification to convert
         *
         * @return {BrickSpriteColor}      the brick color that associates with the color specification
         */
        private convertColorString (spec : BrickColorString) : BrickSpriteColor
        {
            switch (spec)
            {
                case "1": return BrickSpriteColor.BLACK;
                case "0": return BrickSpriteColor.WHITE;
                case "R": return BrickSpriteColor.RED;
                case "O": return BrickSpriteColor.ORANGE;
                case "Y": return BrickSpriteColor.YELLOW;
                case "G": return BrickSpriteColor.GREEN;
                case "B": return BrickSpriteColor.BLUE;
                case "P": return BrickSpriteColor.PINK;
                default:
                    return BrickSpriteColor.NONE;
            }
        }

        /**
         * Reset the grid of bricks (optionally to the given layout), getting it back into a play ready state.
         * As a part of the reset, the number of bricks remaining to get until the level ends is also set to
         * the appropriate value, depending on the layout used.
         *
         * An optional layout can be provided, which will be used to set the brick layout. In the absence of a
         * layout being given, a default debug layout will be used instead.
         *
         * The TypeScript compiler cannot guarantee that the layout contains the correct number of rows and
         * columns, only that the actual brick specifications are valid. As such, this uses a lazy validation
         * style wherein it is careful to not break things too badly.
         *
         * In particular, extra rows and columns are ignored and if there are not enough rows, the remaining
         * rows are filled out with blank spaces. When it comes to columns, if there are not enough in a row,
         * the data from the next row will be used to complete the current row, which may leave the next row
         * short, and so on. Basically, be more careful of columns than rows if you want your layouts to look
         * more or less correct.
         *
         * @param {BrickLayout} layout The layout to use (optional); if not provided, a debug layout is used
         * instead.
         */
        reset (layout? : BrickLayout) : void
        {
            // Reset the number of bricks that are remaining to be destroyed to 0 so that we can accurately
            // reflect the number of bricks in the layout.
            this._bricksLeft = 0;

            // If we were given a layout, then use it.
            if (layout)
            {
                // This will store the index of the current brick in our internal array.
                let index = 0;

                // Our layout is an array of rows of bricks, where each row is itself an array of strings that
                // represent brick color.
                //
                // Iterate over the rows, taking care to stop if there are not enough or there are too many.
                for (let row = 0 ; row < layout.length && row < BRICK_ROWS ; row++)
                {
                    // Pull out the actual row data and iterate it as we did above, making sure we stop if
                    // we have too much or too little data.
                    let rowData = layout[row];
                    for (let col = 0 ; col < rowData.length && col < BRICK_COLS ; col++, index++)
                    {
                        // Get this brick and reset its internal state back to defaults; this makes it an
                        // undamaged empty brick with a single point of life.
                        let brick = this._bricks[index];
                        brick.reset ();

                        // Now we can get the color for this brick and set it. Set up properties on the brick
                        // based on its new color.
                        brick.color = this.convertColorString (rowData [col]);
                        switch (brick.color)
                        {
                            // Red bricks are harder to destroy.
                            case BrickSpriteColor.RED:
                                brick.maxLife = 3;
                                brick.life = 3;
                                break;

                            // Black bricks are invincible.
                            case BrickSpriteColor.BLACK:
                                brick.isInvincible = true;
                                break;
                        }

                        // Bricks that are not empty and are not invincible count as bricks that need to be
                        // destroyed before the level is complete.
                        if (brick.color != BrickSpriteColor.NONE && brick.isInvincible == false)
                            this._bricksLeft++;
                    }
                }

                // Since we can't ensure at compile time that the columns and rows of data are the correct
                // size, here we will just fill up the rest of the space with blanks by resetting those
                // bricks.
                for ( ; index < BRICK_COLS * BRICK_ROWS ; index++)
                    this._bricks[index].reset ();
            }

            // No layout is given (this is the fallback); just fill the grid in with a layout of incrementing
            // brick color based on position.
            else
            {
                // Fill in the grid with bricks. We set up each row to be a single brick color with some
                // simple math, constraining the value to the list of value sprite values.
                for (let index = 0; index < BRICK_COLS * BRICK_ROWS ; index++)
                {
                    // Reset this brick back to defaults, then change the color.
                    this._bricks[index].reset ();
                    this._bricks[index].color = Math.floor(index / BRICK_COLS) % BrickSpriteColor.SPRITE_COUNT;
                    this._bricksLeft++;
                }
            }
        }

        /**
         * Validate that the column and row provided are in range, and if so, return the brick entity for that
         * location; such a brick may still be the empty brick; this just gets you the actual brick to do with
         * as you will.
         *
         * When the location is invalid, null is returned
         *
         * @param column the column to get the brick for
         * @param row    the row to get the brick for
         *
         * @return {Brick} the brick at this location (which might be of color NONE), or null if the location
         * is invalid.
         */
        getBrick (column : number, row : number) : Brick
        {
            // Return the brick constant at this position.
            if (column >= 0 && row >= 0 && row < BRICK_ROWS && column < BRICK_COLS)
                return this._bricks[row * BRICK_COLS + column];

            // No brick here.
            return null;
        }

        /**
         * Given a row and column specification, return a boolean indicating if that position in the brick
         * grid currently contains a brick that is visible or not.
         *
         * Essentially this checks to make sure that the location is actually inside the grid, then collects
         * the brick entity at that location and THEN queries it to see what it's color is, and only returns
         * true when there is a brick and its color is not NONE.
         *
         * This doesn't take brick life into account, just the visibility (as the name suggests).
         *
         * @param column the column to check for bricks
         * @param row    the row to check for bricks
         *
         * @return {boolean} true if there is a brick at the given location and it is visible, or false if
         * there is not or the position provided is out of range
         */
        visibleBrickAtColRow (column : number, row : number) : boolean
        {
            // There is a brick at this location if the location is valid and the color of it is not none.
            let brick = this.getBrick (column, row);
            return brick != null && brick.color != BrickSpriteColor.NONE;
        }

        /**
         * Given a row and column specification, remove any brick that might exist in that position in the
         * grid.
         *
         * WHen either the column or the row are out of bounds of the size of the grid, this silently does
         * nothing. Additionally, this silently does nothing if the specified grid position is already empty
         * of bricks.
         *
         * @param column the column to remove the brick for
         * @param row    the row to remove the brick for
         */
        removeBrick (column : number, row : number) : void
        {
            // If there is a brick at the given location, then we should remove it now.
            if (this.visibleBrickAtColRow (column, row))
            {
                // Get the brick and make it invisible.
                let brick = this.getBrick (column, row);
                brick.color = BrickSpriteColor.NONE;

                // Now count it as a brick removed.
                this._bricksLeft--;
            }
        }

        /**
         * This is called every frame update (tick tells us how many times this has happened) to allow us
         * to update our position.
         *
         * @param stage the stage that we are on
         * @param tick the current engine tick; this advances one for each frame update
         */
        update (stage : Stage, tick : number) : void
        {
            // Do nothing; this could be used to animate the bricks.
        }

        /**
         * Render the brick grid.
         *
         * @param x the x of the location to render at
         * @param y the y of the location to render at
         * @param renderer the renderer to blit with
         */
        render (x : number, y : number, renderer : Renderer) : void
        {
            // Iterate over all rows and columns.
            for (let row = 0, blitY = y ; row < BRICK_ROWS ; row++, blitY += this._brickSheet.height)
            {
                for (let col = 0, blitX = x ; col < BRICK_COLS ; col++, blitX += this._brickSheet.width)
                {
                    // Get the brick at this location; if it's not an empty position, render it.
                    let brick = this._bricks[row * BRICK_COLS + col];
                    brick.render (blitX, blitY, renderer);
                }
            }
        }
    }
}
