var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var nurdz;
(function (nurdz) {
    var game;
    (function (game) {
        /**
         * This specifies the number of points that need to be scored in order to earn an extra life.
         *
         * This value is treated as cumulative with itself.
         *
         * @type {Number}
         */
        game.EXTRA_LIFE_SCORE = 30000;
        /**
         * The storage object in the browser that we should use to store persistent information; the value here
         * is null if local storage is not supported on this device or if it is but is currently turned off (e.g.
         * private browsing).
         *
         * @type {Storage}
         */
        var storage = getLocalStorage();
        /**
         * The current score of the game. This increases every time the ball removes a brick or bricks from the
         * play area. This resets at the start of a new game.
         *
         * @type {number}
         */
        game.score = 0;
        /**
         * The highest score that has been achieved so far; this gets stored in local storage (if the browser
         * supports it) and restored at the start of the next session.
         *
         * When it's 0, the high score is not reported anywhere.
         *
         * @type {number}
         */
        game.highScore = 0;
        /**
         * Whether or not music should be muted or not; this gets stored in local storage (if the browser
         * supports it) and restored at the start of the next session so that the mute state tracks from one
         * session to another.
         *
         * @type {boolean}
         */
        game.musicMute = false;
        /**
         * The number of "lives" the player has. This decreases by one every time the ball falls off the bottom of
         * the screen and resets. When this hits 0, everything resets back to its initial state and the game goes
         * again.
         *
         * @type {number}
         */
        game.lives = 3;
        /**
         * This is an array of the colors that are used primarily for the bricks in the game. This is used to
         * construct a gradient for the title text that will allow us to use all of the colors at once as well as
         * other uses.
         *
         * @type {Array<string>}
         */
        game.brickColors = ['#494949', '#e5e5e5', '#c83e3e', '#e86a17',
            '#ffcc00', '#80be1f', '#1ea7e1', '#ff99cc'];
        /**
         * This defines the different types of music that all of the scenes share with each other. The values in
         * the enumeration are used as array indexes to select the appropriate global music.
         *
         * @type {[type]}
         */
        (function (GameMusic) {
            GameMusic[GameMusic["MUSIC_TITLE"] = 0] = "MUSIC_TITLE";
            GameMusic[GameMusic["MUSIC_GAME"] = 1] = "MUSIC_GAME";
            GameMusic[GameMusic["MUSIC_GAMEOVER"] = 2] = "MUSIC_GAMEOVER";
        })(game.GameMusic || (game.GameMusic = {}));
        var GameMusic = game.GameMusic;
        ;
        /**
         * Given a stage and an image, return a version of the image that has been tiled/constrained to the
         * size of the provided stage.
         *
         * In particular, when the image provided is exactly the dimensions of the stage, it is returned back. In
         * all other cases (including being larger than the stage) a new image is constructed using the dimensions
         * of the stage provided and the image is tiled onto it, potentially performing a crop.
         *
         * The resulting image is then returned.
         *
         * @param  stage the stage the image should take its size from
         * @param  image the image to file
         *
         * @return an image that is potentially a tiled or cropped version of the passed in image
         */
        function tileImage(stage, image) {
            // If the incoming image is already the size of the stage, it's find the way that it is.
            // If the image is fine, use it as is.
            if (image.width == stage.width && image.height == stage.height)
                return image;
            // Get the renderer from the stage.
            var renderer = stage.renderer;
            // Save the current contents of the canvas so that we can restore it.
            var stageContents = renderer.context.getImageData(0, 0, stage.width, stage.height);
            // Tile the image we got across the stage
            for (var blitY = 0; blitY < stage.height; blitY += image.height) {
                for (var blitX = 0; blitX < stage.width; blitX += image.width)
                    renderer.blit(image, blitX, blitY);
            }
            // Now convert the canvas into an image; as far as I can tell this is the only way to do it.
            var tiledImg = document.createElement("img");
            tiledImg.src = stage.canvas.toDataURL("image/png");
            // Now put the original stage contents back where we got it and make sure it gets collected.
            renderer.context.putImageData(stageContents, 0, 0);
            stageContents = null;
            return tiledImg;
        }
        game.tileImage = tileImage;
        /**
         * Perform a check to see if the current browser supports local storage. This tests not only that the
         * feature exists, but that it is currently enabled and usable.
         *
         * @return {Boolean} true if the current browser supports local storage and it is enabled, or false
         * otherwise
         */
        /**
         * Perform a check to see if the current browser supports local storage and that it is actually enabled
         * and can save and restore data.
         *
         * If the check passes, the function returns the storage object to use; otherwise it returns null.
         *
         * @return {Storage} [description]
         */
        function getLocalStorage() {
            try {
                // Pull the local storage object from the window, if any. This has to be inside of the exception
                // handler because in some cases this can throw an exception.
                var storage_1 = window['localStorage'];
                var tmp = '_____storage_test_____';
                var result = void 0;
                // Try to set the temporary value in using itself as a key, then extract the value.
                storage_1.setItem(tmp, tmp);
                result = storage_1.getItem(tmp) == tmp;
                // Remove the item now.
                storage_1.removeItem(tmp);
                // If the result is a true value, then everything worked, so return the storage object.
                if (result)
                    return storage_1;
            }
            catch (e) {
            }
            // There is no support, or there is but we're size restricted.
            return null;
        }
        /**
         * Try to load a previously saved high score from local storage, if it is enabled on this browser and has
         * data stored.
         *
         * When this is not the case (no local storage, no data stored yet) the returned high score will be 0.
         *
         * @return {Number} the last known high score, or 0 if we don't have one
         */
        function loadHighScore() {
            // Is there a storage object to fetch data from?
            if (storage != null) {
                // Collect the data from local storage and try to parse it into an integer. If that works, we can
                // return the value.
                var result = storage.getItem("highScore");
                if (result != null) {
                    // Parse the string back into a number; this returns NaN for things that are not a number.
                    result = parseInt(result, 10);
                    if (isNaN(result) == false)
                        return result;
                }
            }
            // We can't load a high score, so just return 0 in this case to turn that feature off for the time
            // being, until a game is played locally.
            return 0;
        }
        game.loadHighScore = loadHighScore;
        /**
         * Persist the current high score to local storage, so that when the user returns it's still set.
         *
         * If there is no current local storage, this does nothing.
         */
        function saveHighScore() {
            // If we have a local storage, store the current high score there.
            if (storage != null)
                storage.setItem("highScore", game.highScore + "");
        }
        game.saveHighScore = saveHighScore;
        /**
         * Try to load a previously saved mute state for music from local storage, if it is enabled on this
         * browser and has data stored.
         *
         * When this is not the case (no local storage, no data stored yet), the returned mute state will be false
         * (i.e. music will play)
         *
         * @return {boolean} the last known mute state for music, or false if we don't have one
         */
        function loadMusicMute() {
            // Is there a storage item to fetch data from?
            if (storage != null) {
                // Local storage only stored strings, so we rely on the stored value being the exact string "true"
                // to indicate mute is on and anything else to be false.
                var result = storage.getItem("musicMute");
                if (result != null)
                    return result === "true";
            }
            // We can't load the mute state so just return false to indicate that we want music for the time
            // being, until the user turns it off again (if they want to).
            return false;
        }
        game.loadMusicMute = loadMusicMute;
        /**
         * Persist the current music mute state to local storage, so that when the user returns it's still set.
         *
         * If there is no current local storage, this does nothing.
         */
        function saveMusicMute() {
            // If we have a local storage, store the value there
            if (storage != null)
                storage.setItem("musicMute", game.musicMute.toString());
        }
        game.saveMusicMute = saveMusicMute;
        /**
         * Render the current state of music (muted or playing) onto the provided stage. This renders the
         * appropriate sprite at the appropriate location on the screen.
         *
         * @param {Stage} stage the stage to render onto
         */
        function renderMuteState(stage) {
            // Render the mute state in the lower left corner.
            game.speakerSheet.blit(game.musicMute ? 1 : 0, 16, stage.height - 16 - game.speakerSheet.height, stage.renderer);
        }
        game.renderMuteState = renderMuteState;
    })(game = nurdz.game || (nurdz.game = {}));
})(nurdz || (nurdz = {}));
var nurdz;
(function (nurdz) {
    var game;
    (function (game) {
        /**
         * How many columns of bricks the play area contains.
         *
         * @type {number}
         */
        var BRICK_COLS = 10;
        /**
         * How many rows of bricks the play area contains.
         *
         * @type {number}
         */
        var BRICK_ROWS = 16;
        /**
         * How many pixels the bricks render from the top of the screen; this makes room for things like the score
         * to be displayed above the grid.
         *
         * @type {number}
         */
        var BRICK_RENDER_OFFSET = 50;
        /**
         * This entity represents the grid of bricks in the game.
         */
        var BrickGrid = (function (_super) {
            __extends(BrickGrid, _super);
            /**
             * Construct a new brick grid that will render on the stage provided.
             *
             * @param stage the stage the brick grid will be on
             */
            function BrickGrid(stage) {
                // Invoke the super. We don't have a set size but our position is set to assume that the top is
                // where the predefined render offset is located.
                //
                // Sometime in the future this might want to set up the width and height based on knowing the
                // brick size and the grid size; for right now, we assume that the predefined grid size works with
                // the brick size in order to exactly fill the desired area.
                _super.call(this, "brickGrid", stage, 0, BRICK_RENDER_OFFSET, 0, 0, 1, {}, {}, 'red');
                // Load the sprite sheet for the brick images.
                this._brickSheet = new game.SpriteSheet(stage, "bricks_1_8.png", 1, game.BrickSpriteColor.SPRITE_COUNT, true);
                // Initialize our underlying array of bricks. This just needs to be some empty brick instances;
                // they will get their properties later.
                this._bricks = new Array(BRICK_COLS * BRICK_ROWS);
                for (var i = 0; i < BRICK_COLS * BRICK_ROWS; i++)
                    this._bricks[i] = new game.Brick(stage);
                this._bricksLeft = 0;
            }
            Object.defineProperty(BrickGrid.prototype, "gridRows", {
                /**
                 * The number of rows of bricks that can appear (at most) in the brick grid.
                 */
                get: function () { return BRICK_ROWS; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BrickGrid.prototype, "gridColumns", {
                /**
                 * The number of columns of bricks that can appear (at most) in the brick grid.
                 */
                get: function () { return BRICK_COLS; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BrickGrid.prototype, "bricksLeft", {
                /**
                 * The number of bricks remaining in the grid; when this is 0, the grid is completely empty.
                 */
                get: function () { return this._bricksLeft; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BrickGrid.prototype, "brickWidth", {
                /**
                 * How wide each brick is, in pixels.
                 */
                get: function () { return this._brickSheet.width; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BrickGrid.prototype, "brickHeight", {
                /**
                 * How tall each brick is, in pixels.
                 */
                get: function () { return this._brickSheet.height; },
                enumerable: true,
                configurable: true
            });
            /**
             * Given a brick color string, return back the sprite color of the brick that associates with that
             * particular specification/
             *
             * @param  {BrickColorString} spec the color specification to convert
             *
             * @return {BrickSpriteColor}      the brick color that associates with the color specification
             */
            BrickGrid.prototype.convertColorString = function (spec) {
                switch (spec) {
                    case "1": return game.BrickSpriteColor.BLACK;
                    case "0": return game.BrickSpriteColor.WHITE;
                    case "R": return game.BrickSpriteColor.RED;
                    case "O": return game.BrickSpriteColor.ORANGE;
                    case "Y": return game.BrickSpriteColor.YELLOW;
                    case "G": return game.BrickSpriteColor.GREEN;
                    case "B": return game.BrickSpriteColor.BLUE;
                    case "P": return game.BrickSpriteColor.PINK;
                    default:
                        return game.BrickSpriteColor.NONE;
                }
            };
            /**
             * Reset the grid of bricks (optionally to the given layout), getting it back into a play ready state.
             * As a part of the reset, the number of bricks remaining to get until the level ends is also set to
             * the appropriate value, depending on the layout used.
             *
             * An optional layout can be provided, which will be used to set the brick layout. In the absence of a
             * layout being given, a default debug layout will be used instead.
             *
             * The TypeScript compiler cannot guarantee that the layout contains the correct number of rows and
             * columns, only that the actual brick specifications are valid. As such, this uses a lazy validation
             * style wherein it is careful to not break things too badly.
             *
             * In particular, extra rows and columns are ignored and if there are not enough rows, the remaining
             * rows are filled out with blank spaces. When it comes to columns, if there are not enough in a row,
             * the data from the next row will be used to complete the current row, which may leave the next row
             * short, and so on. Basically, be more careful of columns than rows if you want your layouts to look
             * more or less correct.
             *
             * @param {BrickLayout} layout The layout to use (optional); if not provided, a debug layout is used
             * instead.
             */
            BrickGrid.prototype.reset = function (layout) {
                // Reset the number of bricks that are remaining to be destroyed to 0 so that we can accurately
                // reflect the number of bricks in the layout.
                this._bricksLeft = 0;
                // If we were given a layout, then use it.
                if (layout) {
                    // This will store the index of the current brick in our internal array.
                    var index = 0;
                    // Our layout is an array of rows of bricks, where each row is itself an array of strings that
                    // represent brick color.
                    //
                    // Iterate over the rows, taking care to stop if there are not enough or there are too many.
                    for (var row = 0; row < layout.length && row < BRICK_ROWS; row++) {
                        // Pull out the actual row data and iterate it as we did above, making sure we stop if
                        // we have too much or too little data.
                        var rowData = layout[row];
                        for (var col = 0; col < rowData.length && col < BRICK_COLS; col++, index++) {
                            // Get this brick and reset its internal state back to defaults; this makes it an
                            // undamaged empty brick with a single point of life.
                            var brick = this._bricks[index];
                            brick.reset();
                            // Now we can get the color for this brick and set it. Set up properties on the brick
                            // based on its new color.
                            brick.color = this.convertColorString(rowData[col]);
                            switch (brick.color) {
                                // Red bricks are harder to destroy.
                                case game.BrickSpriteColor.RED:
                                    brick.maxLife = 3;
                                    brick.life = 3;
                                    break;
                                // Black bricks are invincible.
                                case game.BrickSpriteColor.BLACK:
                                    brick.isInvincible = true;
                                    break;
                            }
                            // Bricks that are not empty and are not invincible count as bricks that need to be
                            // destroyed before the level is complete.
                            if (brick.color != game.BrickSpriteColor.NONE && brick.isInvincible == false)
                                this._bricksLeft++;
                        }
                    }
                    // Since we can't ensure at compile time that the columns and rows of data are the correct
                    // size, here we will just fill up the rest of the space with blanks by resetting those
                    // bricks.
                    for (; index < BRICK_COLS * BRICK_ROWS; index++)
                        this._bricks[index].reset();
                }
                else {
                    // Fill in the grid with bricks. We set up each row to be a single brick color with some
                    // simple math, constraining the value to the list of value sprite values.
                    for (var index = 0; index < BRICK_COLS * BRICK_ROWS; index++) {
                        // Reset this brick back to defaults, then change the color.
                        this._bricks[index].reset();
                        this._bricks[index].color = Math.floor(index / BRICK_COLS) % game.BrickSpriteColor.SPRITE_COUNT;
                        this._bricksLeft++;
                    }
                }
            };
            /**
             * Validate that the column and row provided are in range, and if so, return the brick entity for that
             * location; such a brick may still be the empty brick; this just gets you the actual brick to do with
             * as you will.
             *
             * When the location is invalid, null is returned
             *
             * @param column the column to get the brick for
             * @param row    the row to get the brick for
             *
             * @return {Brick} the brick at this location (which might be of color NONE), or null if the location
             * is invalid.
             */
            BrickGrid.prototype.getBrick = function (column, row) {
                // Return the brick constant at this position.
                if (column >= 0 && row >= 0 && row < BRICK_ROWS && column < BRICK_COLS)
                    return this._bricks[row * BRICK_COLS + column];
                // No brick here.
                return null;
            };
            /**
             * Given a row and column specification, return a boolean indicating if that position in the brick
             * grid currently contains a brick that is visible or not.
             *
             * Essentially this checks to make sure that the location is actually inside the grid, then collects
             * the brick entity at that location and THEN queries it to see what it's color is, and only returns
             * true when there is a brick and its color is not NONE.
             *
             * This doesn't take brick life into account, just the visibility (as the name suggests).
             *
             * @param column the column to check for bricks
             * @param row    the row to check for bricks
             *
             * @return {boolean} true if there is a brick at the given location and it is visible, or false if
             * there is not or the position provided is out of range
             */
            BrickGrid.prototype.visibleBrickAtColRow = function (column, row) {
                // There is a brick at this location if the location is valid and the color of it is not none.
                var brick = this.getBrick(column, row);
                return brick != null && brick.color != game.BrickSpriteColor.NONE;
            };
            /**
             * Given a row and column specification, remove any brick that might exist in that position in the
             * grid.
             *
             * WHen either the column or the row are out of bounds of the size of the grid, this silently does
             * nothing. Additionally, this silently does nothing if the specified grid position is already empty
             * of bricks.
             *
             * @param column the column to remove the brick for
             * @param row    the row to remove the brick for
             */
            BrickGrid.prototype.removeBrick = function (column, row) {
                // If there is a brick at the given location, then we should remove it now.
                if (this.visibleBrickAtColRow(column, row)) {
                    // Get the brick and make it invisible.
                    var brick = this.getBrick(column, row);
                    brick.color = game.BrickSpriteColor.NONE;
                    // Now count it as a brick removed.
                    this._bricksLeft--;
                }
            };
            /**
             * This is called every frame update (tick tells us how many times this has happened) to allow us
             * to update our position.
             *
             * @param stage the stage that we are on
             * @param tick the current engine tick; this advances one for each frame update
             */
            BrickGrid.prototype.update = function (stage, tick) {
                // Do nothing; this could be used to animate the bricks.
            };
            /**
             * Render the brick grid.
             *
             * @param x the x of the location to render at
             * @param y the y of the location to render at
             * @param renderer the renderer to blit with
             */
            BrickGrid.prototype.render = function (x, y, renderer) {
                // Iterate over all rows and columns.
                for (var row = 0, blitY = y; row < BRICK_ROWS; row++, blitY += this._brickSheet.height) {
                    for (var col = 0, blitX = x; col < BRICK_COLS; col++, blitX += this._brickSheet.width) {
                        // Get the brick at this location; if it's not an empty position, render it.
                        var brick = this._bricks[row * BRICK_COLS + col];
                        brick.render(blitX, blitY, renderer);
                    }
                }
            };
            return BrickGrid;
        }(game.Entity));
        game.BrickGrid = BrickGrid;
    })(game = nurdz.game || (nurdz.game = {}));
})(nurdz || (nurdz = {}));
var nurdz;
(function (nurdz) {
    var game;
    (function (game) {
        /**
         * This is used to provide a human readable sprite number to the different colors of brick sprites that
         * exist in the brick sprite sheet.
         */
        (function (BrickSpriteColor) {
            // There is no brick; this spot is empty and does not render/block the ball.
            BrickSpriteColor[BrickSpriteColor["NONE"] = -1] = "NONE";
            // The sprite colors, in order
            BrickSpriteColor[BrickSpriteColor["BLACK"] = 0] = "BLACK";
            BrickSpriteColor[BrickSpriteColor["WHITE"] = 1] = "WHITE";
            BrickSpriteColor[BrickSpriteColor["RED"] = 2] = "RED";
            BrickSpriteColor[BrickSpriteColor["ORANGE"] = 3] = "ORANGE";
            BrickSpriteColor[BrickSpriteColor["YELLOW"] = 4] = "YELLOW";
            BrickSpriteColor[BrickSpriteColor["GREEN"] = 5] = "GREEN";
            BrickSpriteColor[BrickSpriteColor["BLUE"] = 6] = "BLUE";
            BrickSpriteColor[BrickSpriteColor["PINK"] = 7] = "PINK";
            BrickSpriteColor[BrickSpriteColor["SPRITE_COUNT"] = 8] = "SPRITE_COUNT";
        })(game.BrickSpriteColor || (game.BrickSpriteColor = {}));
        var BrickSpriteColor = game.BrickSpriteColor;
        ;
        /**
         * This entity represents the ball in the game.
         */
        var Brick = (function (_super) {
            __extends(Brick, _super);
            /**
             * Construct a new ball that will render on the stage provided.
             *
             * @param stage the stage the ball will be on
             */
            function Brick(stage) {
                // Invoke the super. Note that we don't provide any location or dimensions here. These will
                // get set later.
                _super.call(this, "brick", stage, 0, 0, 0, 0, 1, {}, {}, 'red');
                // Load our sprite sheet
                this._sheet = new game.SpriteSheet(stage, "bricks_1_8.png", 1, BrickSpriteColor.SPRITE_COUNT, true);
                this._crackSheet = new game.SpriteSheet(stage, "brick_cracks_1_2.png", 1, 2, true);
                // Reset intenal state.
                this.reset();
            }
            Object.defineProperty(Brick.prototype, "color", {
                /**
                 * Get the color that this brick renders with.
                 *
                 * @return {BrickSpriteColor} the current brick color
                 */
                get: function () { return this._sprite; },
                /**
                 * Set the color that this brick renders with
                 *
                 * @param {BrickSpriteColor} newColor the new color
                 */
                set: function (newColor) { this._sprite = newColor; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Brick.prototype, "isInvincible", {
                /**
                 * Get the current invincibiliy state for this brick; when it is invincible, collisions with the ball
                 * do not have any effect on it.
                 *
                 * @return {boolean} true if this brick is currently invincible, or false otherwise.
                 */
                get: function () { return this._invincible; },
                /**
                 * Change the current invincible state for this brick; when it is invincible, collisions with the
                 * ball do not have any effect on it.
                 *
                 * @param {boolean} newStatus true if this brick should be invincible, or false otherwise.
                 */
                set: function (newStatus) { this._invincible = newStatus; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Brick.prototype, "maxLife", {
                /**
                 * Get the maximum amount of life that this brick should have. This is the amount of life that the
                 * brick started with, which might be different than the current life level.
                 *
                 * @return {number} the maximum life value for this brick
                 */
                get: function () { return this._maxLife; },
                /**
                 * Set the maximum amount of life that this brick should have. This is used along with the life value
                 * to determine if this brick should appear damaged or not.
                 *
                 * @param {number} newMax the new maximum life value
                 */
                set: function (newMax) { this._maxLife = newMax; },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Brick.prototype, "life", {
                /**
                 * Get the current life of this brick; this is the number of hits needed on this brick before it will
                 * be removed from the grid. This value may be different from the maximum life of the brick.
                 *
                 * @return {number} the current life of this brick; 0 if the brick is currently gone
                 */
                get: function () { return this._life; },
                /**
                 * Set the life that this brick should have; this is an indication of how many impacts with the ball
                 * the brick needs before it is actually removed from the grid.
                 *
                 * As long as the currently set life value is smaller than the maximum life value currently set, this
                 * brick will render itself as damaged.
                 *
                 * @param {number} newLife the new life value
                 */
                set: function (newLife) {
                    // The life value can't change if this brick is invincible
                    if (this._invincible)
                        return;
                    // Set the new life value now.
                    this._life = newLife;
                    // The brick is damaged now. If it was not before, select a random starting crack sprite for it.
                    // Otherwise, skip to the next sprite so that there is a visual indication that something happend.
                    if (this._life < this._maxLife) {
                        if (this._crackSprite < 0)
                            this._crackSprite = game.Utils.randomIntInRange(0, this._crackSheet.count - 1);
                        else
                            this._crackSprite = (this._crackSprite + 1) % this._crackSheet.count;
                    }
                    else
                        this._crackSprite = -1;
                },
                enumerable: true,
                configurable: true
            });
            /**
             * Reset this brick back to its default values. This sets the brick to be invisible, with a single
             * life point and no visible damage.
             */
            Brick.prototype.reset = function () {
                // Sprite is now empty with a single life point and no visible damage. It is also not invincible.
                this._sprite = BrickSpriteColor.NONE;
                this._invincible = false;
                this._crackSprite = -1;
                this._maxLife = 1;
                this._life = 1;
            };
            /**
             * Render the brick at the provided location and using the given renderer.
             *
             * @param x the x co-ordinate of the brick
             * @param y the y co-ordinate of the brick
             * @param renderer the renderer to blit with
             */
            Brick.prototype.render = function (x, y, renderer) {
                // Leave if we have no sprite assigned; we should not render.
                if (this._sprite == BrickSpriteColor.NONE)
                    return;
                // Use the super to do our render for us; it will use our sprite to render as we see fit. Then
                // render our crack sprite, if we are currently damaged.
                _super.prototype.render.call(this, x, y, renderer);
                if (this._crackSprite >= 0)
                    renderer.blitSprite(this._crackSheet, this._crackSprite, x, y);
            };
            return Brick;
        }(game.Entity));
        game.Brick = Brick;
    })(game = nurdz.game || (nurdz.game = {}));
})(nurdz || (nurdz = {}));
var nurdz;
(function (nurdz) {
    var game;
    (function (game) {
        /**
         * When we impact with the paddle, our X speed is modified based on how far away from the center of
         * the paddle we are (the farther, the faster). This value is used to damp that value down so that the
         * ball doesn't go too crazy at the outer bounds of the paddle.
         *
         * @type {number}
         */
        game.BALL_PADDLE_SPEED_DAMPER = 0.35;
        /**
         * This is used to provide a human readable sprite number to the different colors of ball sprites that
         * exist in the ball sprite sheet.
         */
        var BallSpriteColor;
        (function (BallSpriteColor) {
            BallSpriteColor[BallSpriteColor["WHITE"] = 0] = "WHITE";
            BallSpriteColor[BallSpriteColor["BLACK"] = 1] = "BLACK";
            BallSpriteColor[BallSpriteColor["BLUE"] = 2] = "BLUE";
            BallSpriteColor[BallSpriteColor["YELLOW"] = 3] = "YELLOW";
            BallSpriteColor[BallSpriteColor["SPRITE_COUNT"] = 4] = "SPRITE_COUNT";
        })(BallSpriteColor || (BallSpriteColor = {}));
        ;
        /**
         * This entity represents the ball in the game.
         */
        var Ball = (function (_super) {
            __extends(Ball, _super);
            /**
             * Construct a new ball that will render on the stage provided.
             *
             * @param stage the stage the ball will be on
             */
            function Ball(stage) {
                var _this = this;
                // Invoke the super. Note that we don't provide any location or dimensions here. These will
                // get set later.
                _super.call(this, "ball", stage, 0, 0, 0, 0, 1, {}, {}, 'red');
                /**
                 * The entity that the ball is parented to, if any. While the ball has a parent, its update method
                 * does not do anything motion related (including collision detection).
                 *
                 * Also, while there is a parent set, the position property of the entity is interpreted as being
                 * relative to the parent entity instead of being an absolute screen position. The code that sets
                 * and resets the parent takes care of modifying the position property so that the current position of
                 * the ball on the screen is captured.
                 *
                 * @type {Entity}
                 */
                this._parent = null;
                /**
                 * Set the dimensions of the ball sprite based on the sprite sheet provided
                 *
                 * @param sheet the sprite sheet that was loaded
                 */
                this.setDimensions = function (sheet) {
                    // Set our dimensions to be circular and based on the dimensions of the sprites in the sheet.
                    _this.makeCircle(sheet.width / 2, true);
                };
                // Set up the sprite sheet that contain our ball images. THe size of the entity is determined by
                // the size of the sprites, so we let the callback handle that.
                this._sheet = new game.SpriteSheet(stage, "ball_4_1.png", BallSpriteColor.SPRITE_COUNT, 1, true, this.setDimensions);
                this._sprite = BallSpriteColor.YELLOW;
                // Set up a default velocity vector and a speed.
                this._velocity = new game.Vector2D(1, 0);
                this._speed = 1;
                // Initially, there is no listener.
                this._listener = null;
                // Now we can reset the ball.
                this.reset(1);
            }
            Object.defineProperty(Ball.prototype, "listener", {
                /**
                 * Get the object that will get told of events for this ball when they occur.
                 *
                 * @return {BallEventListener} the current ball event listener for this ball, or null if there is
                 * none.
                 */
                get: function () {
                    return this._listener;
                },
                /**
                 * Set the object that will get told of events for this ball when they occur.
                 *
                 * @param {BallEventListener} newListener the new listener
                 */
                set: function (newListener) {
                    this._listener = newListener;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Ball.prototype, "parent", {
                /**
                 * Get the entity that is currently set to be our parent, or null if there is not a parent set.
                 *
                 * While there is a parent set, the ball does not do any updates regarding position or collision
                 * detection, and its position is relative to this parent entity.
                 *
                 * @return {Entity} the entity that we're currently parented to, or null if there isn't one.
                 */
                get: function () {
                    return this._parent;
                },
                /**
                 * Set the entity that we are currently parented to, which can be null to break any current
                 * child/parent relationship. When we have a parent, our position becomes relative to the parent
                 * instead of to the stage.
                 *
                 * This takes care of adjusting the current position so that it becomes relative to the new parent
                 * or the stage as appropriate, so that the position of the ball remains consistent regardless.
                 *
                 * @param {Entity} newParent the entity to make our parent or null to remove an existing parent
                 */
                set: function (newParent) {
                    // If we are currently locked to a parent, then adjust our position by its position so that we put
                    // ourselves back into an absolute world position based on where the parent currently is.
                    if (this._parent != null)
                        this._position.translate(this._parent.position);
                    // Set or clear the new parent.
                    this._parent = newParent;
                    // If we have a new parent now, then we need to adjust our position so that it becomes relative to
                    // the position of the new parent.
                    if (newParent != null) {
                        this._position.x -= this._parent.position.x;
                        this._position.y -= this._parent.position.y;
                    }
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(Ball.prototype, "speed", {
                /**
                 * Get the speed that the ball is traveling in the direction that it is currently pointing. This is
                 * expressed as the magnitude of a velocity vector
                 *
                 * @return {number} the current speed of the ball
                 */
                get: function () {
                    return this._speed;
                },
                /**
                 * Set the speed that the ball is traveling in the direction that it is currently pointing. This is
                 * expressed as the magnitude of a velocity vector.
                 *
                 * @param {number} newSpeed the new speed for the ball to travel in
                 */
                set: function (newSpeed) {
                    // Save the speed and then adjust the velocity vector to have the correct length.
                    this._speed = newSpeed;
                    this._velocity.magnitude = newSpeed;
                },
                enumerable: true,
                configurable: true
            });
            /**
             * Reset the ball.
             *
             * If an entity is provided, the ball will parent itself to that entity and set its speed to travel upwards.
             * Otherwise, it sets its position to the center of the stage and travels downwards.
             */
            Ball.prototype.reset = function (newSpeed, entity) {
                if (entity === void 0) { entity = null; }
                if (entity) {
                    // Set our parent to the entity provided and move upwards. We use the property setter here
                    // because it does magic for us.
                    this.parent = entity;
                    this._velocity.directionDeg = game.Utils.randomIntInRange(210, 320);
                }
                else {
                    // Reset to the center of the screen and moving downward.
                    this._position.setToXY(nurdz.stage.width / 2, nurdz.stage.height / 2);
                    this._velocity.directionDeg = game.Utils.randomIntInRange(30, 150);
                }
                // Now set the speed to the one passed in and become alive.
                this.speed = newSpeed;
                this._dead = false;
            };
            /**
             * Given an entity that supports the brick grid interface, check to see if the ball is colliding with
             * any of the bricks in the grid or not. The ball can only collide with a single brick on every
             * pass through this function.
             *
             * The return value is null if the ball did not collide with anything or a point which represents
             * the spot (in brick grid coordinates) that a brick was collided with.
             *
             * When this reports a collision, the trajectory of the ball is modified but the grid is left alone
             * so that the caller can take special action; it should probably at least remove the brick.
             *
             * @param brickGrid  The object that represents the brick grid to collide with
             * @returns {Point} the point (in brick grid space) that a brick was collided with, or null if none
             */
            Ball.prototype.checkBrickCollision = function (brickGrid) {
                // Convert the ball Y position by shifting it upwards by the offset that the bricks are offset
                // from the top of the screen, so that we can properly convert the ball coordinates.
                var ballY = this._position.y - brickGrid.position.y;
                // Convert our ball location (which is in screen coordinates) into "brick coordinates" to see
                // where on the grid we currently are.
                var bCol = Math.floor(this._position.x / brickGrid.brickWidth);
                var bRow = Math.floor(ballY / brickGrid.brickHeight);
                // If the current position of the ball is outside of the brick grid, we can return without
                // doing anything further.
                if (bRow < 0 || bCol < 0 || bRow >= brickGrid.gridRows || bCol >= brickGrid.gridColumns)
                    return null;
                // If there is no brick at this position, there is nothing to do.
                if (brickGrid.visibleBrickAtColRow(bCol, bRow) == false)
                    return null;
                // Now calculate the grid position for where we started the frame so that we can determine how far
                // we might have moved.
                var bPrevCol = Math.floor((this._position.x - this._velocity.x) / brickGrid.brickWidth);
                var bPrevRow = Math.floor((ballY - this._velocity.y) / brickGrid.brickHeight);
                var bothTestsFailed = true;
                // If the ball column between now and the previous frame changed, and there is not a brick in
                // the column that we came from, then we should reflect horizontally.
                if (bCol != bPrevCol && brickGrid.visibleBrickAtColRow(bPrevCol, bRow) == false) {
                    this._velocity.flipX();
                    bothTestsFailed = false;
                }
                // If the ball row between now and the previous framer changed, and there is not a brick in
                // the row that we came from, then we should reflect vertically.
                if (bRow != bPrevRow && brickGrid.visibleBrickAtColRow(bCol, bPrevRow) == false) {
                    this._velocity.flipY();
                    bothTestsFailed = false;
                }
                // Each test above checks to see if a bounce in a direction is blocked, and only reflects if
                // it is not. However, in the case where neither of those tests trigger, it means that the
                // ball intersected in a way where it jumped both column and row (i.e. through the diagonal
                // corner without touching a brick on either side).
                //
                // In this case, we want to reflect in both directions and go back where we came from.
                if (bothTestsFailed)
                    this._velocity.reverse();
                // Return the collision spot.
                return new game.Point(bCol, bRow);
            };
            /**
             * Check to see if the current position of the ball is colliding with the provided paddle or not.
             * This assumes that the call is made AFTER the ball has been moved along its speed vector
             *
             * When a collision is detected, the velocity of the ball is adjusted appropriately. Additionally,
             * our position may also be modified so that we don't visually interfere with the paddle.
             * @param paddle
             */
            Ball.prototype.checkPaddleCollision = function (paddle) {
                // TODO Maybe this is not safe in the bottom corners?
                //
                // What exactly happens if the update() method detects that the ball is rebounding off of the
                // wall down by the paddle, and then we get called, collide with the paddle, and decide to
                // back up? Probably nothing good.
                // If we are moving downward and the line between where we used to be and where we are now
                // intersects with the paddle, consider that we touched it.
                if (this._velocity.y > 0 &&
                    paddle.intersectWithSegmentXY(this._position.x - this._velocity.x, this._position.y - this._velocity.y, this._position.x, this._position.y)) {
                    // If there is a listener, tell it we're colliding with this paddle.
                    if (this._listener != null)
                        this._listener.ballPaddle(this, paddle);
                    // Unlike a reflection of the ball off of the sides or a brick, which just changes the
                    // direction of one of the vector components, for the paddle we change the X component of the
                    // vector and and just flip the Y. This allows for some control over the direction the ball
                    // goes.
                    this._velocity.x = (this._position.x - paddle.position.x) * game.BALL_PADDLE_SPEED_DAMPER;
                    this._velocity.flipY();
                    // Changing one of the vector components changes the magnitude, so set it back to what it
                    // should be. The only things that should change the velocity of the ball is the game telling
                    // us to kick our speed up (which does this inherently).
                    this._velocity.magnitude = this._speed;
                }
            };
            /**
             * This is called every frame update (tick tells us how many times this has happened) to allow us
             * to update our position.
             *
             * @param stage the stage that we are on
             * @param tick the current engine tick; this advances one for each frame update
             */
            Ball.prototype.update = function (stage, tick) {
                // Don't update if we're dead.
                if (this._dead)
                    return;
                // If we are locked to an entity, don't update ourselves; we shouldn't move in this case.
                if (this._parent != null)
                    return;
                // We want to update, which we do by moving the ball along its speed vector. First we want to
                // check  to see if moving in this manner would cause the ball to be closer to the edge of the
                // screen than its radius (due to its position being determined by its center).
                var newXPos = this._position.x + this._velocity.x;
                if (newXPos <= this.radius || newXPos >= stage.width - this.radius) {
                    // If we moved the ball using our regular speed vector, it would move us too close to the left
                    // or right edge of the screen. In this case we want our movement to be such that we stop
                    // right when our position puts our edge on the edge of the screen. To do that we determine just
                    // how much we have to move on the X axis in our current direction in order to do that. This
                    // might be 0 if the ball is currently exactly on the bounds we desire.
                    var axialDisplacement = 0;
                    if (newXPos < this._position.x)
                        axialDisplacement = this._position.x - this.radius;
                    else if (newXPos > this._position.x)
                        axialDisplacement = (stage.width - this.radius) - this._position.x;
                    // If we are actually supposed to move, do it; if we happened to start in a position where we
                    // were already exactly where we wanted to be, we don't need to do anything.
                    if (axialDisplacement != 0) {
                        // Make a copy of our speed vector and alter its magnitude so that following it will make
                        // us move exactly as far as we want on the X axis and the proper amount on the Y axis.
                        var shortSpeed = this._velocity.copy();
                        shortSpeed.magnitude = axialDisplacement / Math.cos(shortSpeed.direction);
                        // Now we can translate based on our shorted speed vector.
                        this._position.translate(shortSpeed);
                    }
                }
                else
                    // We can just do a regular translate of the point, this isn't going to put anything off of
                    // the screen
                    this._position.translateXY(this._velocity.x, this._velocity.y);
                // If our Y position shows that we're about to go off the bottom of the screen, make ourselves
                // dead, trigger the event (if we can) that tells this, and leave.
                if (this._position.y >= this._stage.height - this.radius) {
                    this._dead = true;
                    if (this._listener != null)
                        this._listener.ballLost(this);
                    return;
                }
                // Check to see if we should bounce off of the ceiling of the screen. This is adjusted by our
                // current radius, so we don't visually appear to ever go off of the screen on the top.
                if (this._position.y <= this.radius && this._velocity.y < 0) {
                    // If there is a current listener, tell it we're colliding with the wall.
                    if (this._listener != null)
                        this._listener.ballWall(this);
                    // Make a reverse copy of our speed vector and then set its magnitude to how far back we
                    // have to go along it to get our center point to be exactly our radius away from the top
                    // edge of the screen.
                    var revSpeed = this._velocity.copyReversed();
                    revSpeed.magnitude = (this.radius - this._position.y) / Math.sin(revSpeed.direction);
                    // Translate backwards along this new vector to reposition ourselves and then reverse our
                    // Y velocity.
                    this._position.translate(revSpeed);
                    this._velocity.flipY();
                }
                // Similar to what we did for the ceiling, check if we're colliding with the left or right of
                // the stage. This only triggers if we're colliding with the side that we're moving towards.
                if ((this._position.x >= stage.width - this.radius && this._velocity.x > 0) ||
                    (this._position.x <= this.radius && this._velocity.x < 0)) {
                    // If there is a current listener, tell it we're colliding with the wall.
                    if (this._listener != null)
                        this._listener.ballWall(this);
                    // Here we just will flip the X speed. We know from the pre-movement code above that we never
                    // get too close to the screen bounds in the general case, so there's nothing special to do
                    // here.
                    this._velocity.flipX();
                }
            };
            /**
             * Render the ball at the provided location and using the given renderer.
             *
             * We assume our position is at our center.
             *
             * @param x the x of the center of the ball
             * @param y the y of the center of the ball
             * @param renderer the renderer to blit with
             */
            Ball.prototype.render = function (x, y, renderer) {
                // Don't render if we're dead.
                if (this._dead)
                    return;
                // The position we get invoked with represents our current position, but when we are locked to
                // another entity, our position is actually an offset from that entity. As a result, in that case
                // we adjust the position given so we still render in the correct location.
                if (this._parent) {
                    x += this._parent.position.x;
                    y += this._parent.position.y;
                }
                // Now invoke the super to actually render.
                _super.prototype.render.call(this, x, y, renderer);
            };
            return Ball;
        }(game.Entity));
        game.Ball = Ball;
    })(game = nurdz.game || (nurdz.game = {}));
})(nurdz || (nurdz = {}));
var nurdz;
(function (nurdz) {
    var game;
    (function (game) {
        /**
         * The distance from the edge of the screen that the paddles is rendered. Larger values mean farther from
         * the edge of the screen and thus less reaction time.
         *
         * @type {number}
         */
        game.PADDLE_EDGESPACE = 60;
        /**
         * This is used to provide a human readable sprite number to the different colors of paddle sprites that
         * exist in the paddle sprite sheet.
         */
        var PaddleSpriteColor;
        (function (PaddleSpriteColor) {
            PaddleSpriteColor[PaddleSpriteColor["RED"] = 0] = "RED";
            PaddleSpriteColor[PaddleSpriteColor["WHITE"] = 1] = "WHITE";
            PaddleSpriteColor[PaddleSpriteColor["BLUE"] = 2] = "BLUE";
            PaddleSpriteColor[PaddleSpriteColor["SPRITE_COUNT"] = 3] = "SPRITE_COUNT";
        })(PaddleSpriteColor || (PaddleSpriteColor = {}));
        ;
        /**
         * The paddle entity is the entity that controls the paddle used by the player(s). Also included is an
         * AI module that will allow it to attempt to track the ball in order to return a serve.
         */
        var Paddle = (function (_super) {
            __extends(Paddle, _super);
            /**
             * Construct a new paddle handled by the provided stage. The paddle is always initially horizontally
             * centered on the stage.
             *
             * @param stage the stage to render on
             */
            function Paddle(stage) {
                var _this = this;
                // Invoke the super to set ourselves up. We reference our bounds with our origin at the center
                // of ourselves. We always start with our position centered vertically, but we need to be told
                // our horizontal position.
                _super.call(this, "paddle", stage, stage.width / 2, stage.height - game.PADDLE_EDGESPACE, 0, 0, 1, {}, {}, 'red');
                /**
                 * When this is not -1, it's the position that our X position should be set to the next time that
                 * we update. This ensures that the paddle is moved at a set time, since input events can occur at
                 * any time and we want to rigidly control our update loop.
                 */
                this._newPos = -1;
                /**
                 * Set the dimensions of the ball sprite based on the sprite sheet provided
                 *
                 * @param sheet the sprite sheet that was loaded
                 */
                this.setDimensions = function (sheet) {
                    // Set up our dimensions based on the size of the sprites in this sprite sheet, and make sure
                    // that our origin is at our center.
                    _this._width = sheet.width;
                    _this._height = sheet.height;
                    _this._origin.setToXY(_this._width / 2, _this._height / 2);
                };
                // Set up our sprite sheet; when this loads, our dimensions will be set to be the size of the
                // sprites in the sprite sheet.
                this._sheet = new game.SpriteSheet(stage, "paddle_1_3.png", 1, PaddleSpriteColor.SPRITE_COUNT, true, this.setDimensions);
                this._sprite = PaddleSpriteColor.BLUE;
                // We start visible.
                this._visible = true;
                // Set up our dimensions.
                this._width = 100;
                this._height = 10;
                // We want our origin to be at our center.
                this._origin.setToXY(this._width / 2, this._height / 2);
            }
            Object.defineProperty(Paddle.prototype, "visible", {
                /**
                 * Get the current visibiilty state of the paddle; this tells you if it is rendering itself when asked
                 * or not.
                 *
                 * @return {boolean} true if the paddle will render itself when asked, or false otherwise.
                 */
                get: function () { return this._visible; },
                /**
                 * Change the current visibility state of the paddle.
                 *
                 * @param newVisible true to render when asked, or false to remain hidden
                 */
                set: function (newVisible) { this._visible = newVisible; },
                enumerable: true,
                configurable: true
            });
            /**
             * This is invoked every frame to update this paddle.
             *
             * @param stage the stage the paddle is on
             * @param tick the game tick, for timing purposes
             */
            Paddle.prototype.update = function (stage, tick) {
                // If we have a new position, then set it now and remove the flag.
                if (this._newPos != -1) {
                    this._position.x = this._newPos;
                    this._newPos = -1;
                }
            };
            /**
             * Render the paddle at the provided location and using the given renderer.
             *
             * @param x the x of the paddle
             * @param y the y of the paddle
             * @param renderer the renderer to blit with
             */
            Paddle.prototype.render = function (x, y, renderer) {
                // Leave if we're not supposed to be visible.
                if (this._visible == false)
                    return;
                // Now invoke the super to actually render.
                _super.prototype.render.call(this, x, y, renderer);
            };
            /**
             * Jump this paddle directly so that its horizontal position is the position passed in. This does
             * limit checking to ensure the position is set to be as valid as possible.
             *
             * @param newPos the new X position.
             */
            Paddle.prototype.jumpTo = function (newPos) {
                // Set up what our new position should be in the next update, and make sure that it's clamped
                // to an acceptable range.
                this._newPos = game.Utils.clampToRange(newPos, 0, this._stage.width - 1);
            };
            return Paddle;
        }(game.Entity));
        game.Paddle = Paddle;
    })(game = nurdz.game || (nurdz.game = {}));
})(nurdz || (nurdz = {}));
var nurdz;
(function (nurdz) {
    var game;
    (function (game) {
        /**
         * The blinking text entity is an entity that is used to display some blinking text. This allows changing
         * the text displayed, the font used, and the position. The position is always assumed to be the center
         * of the dimensions.
         */
        var BlinkingText = (function (_super) {
            __extends(BlinkingText, _super);
            /**
             * Construct a new blinking text entity to be handled by the provided stage.
             *
             * @param stage the stage to render on
             * @param font the font to display the text in
             * @param text the text to display
             */
            function BlinkingText(stage, font, text) {
                // Invoke the super to set ourselves up. We reference our bounds with our origin at the center
                // of ourselves. We always start with our position centered vertically, but we need to be told
                // our horizontal position.
                _super.call(this, "text", stage, stage.width / 2, stage.height, 0, 0, 1, {}, {}, 'red');
                /**
                 * The list of colors used to blink our text.
                 *
                 * @type {Array<string>}
                 */
                this._blinkColors = ['#ffffff', '#aaaaaa'];
                /**
                 * The current color in _blinkColors that is being used to render blinking text.
                 *
                 * @type {number}
                 */
                this._blinkIndex = 0;
                /**
                 * The number of game ticks that happen before the color changes to the next one in the color list.
                 * The higher the number, the slower the blink speed.
                 *
                 * @type {number}
                 */
                this._blinkTicks = 7;
                // Set the font (raw) and then the text. We set the font raw so that we don't do a duplicate font
                // measurement.
                this._font = font;
                this.text = text;
            }
            Object.defineProperty(BlinkingText.prototype, "text", {
                /**
                 * Obtain the text that this is currently set to display.
                 *
                 * @return {string} the currently set text
                 */
                get: function () {
                    return this._text;
                },
                /**
                 * Change the text that is to be displayed by this entity. This will cause the extents of the entity
                 * to be re-calculated.
                 */
                set: function (newText) {
                    this._text = newText;
                    this.resetDimensions();
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BlinkingText.prototype, "font", {
                /**
                 * Obtain the font that will be used to display this text.
                 */
                get: function () {
                    return this._font;
                },
                /**
                 * Change the font that is to be used to display this entity. This will cause the extents of the
                 * entity to be re-calculated
                 */
                set: function (newFont) {
                    this._font = newFont;
                    this.resetDimensions();
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BlinkingText.prototype, "colors", {
                /**
                 * Get the current set of colors that are used to blink this text.
                 */
                get: function () {
                    return this._blinkColors;
                },
                /**
                 * Change the set of colors that are used to blink this text. This inherently resets the blink
                 * sequence.
                 */
                set: function (newColors) {
                    this._blinkColors = newColors;
                    this._blinkIndex = 0;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(BlinkingText.prototype, "blinkTicks", {
                /**
                 * Get the number of ticks between color changes, which controls the speed of the blink. This counts
                 * ticks as frames, so a value of 30 means the color changes once per second.
                 */
                get: function () {
                    return this._blinkTicks;
                },
                /**
                 * Set the number of ticks between color changes, which controls the speed of the blink. This counts
                 * ticks as frames, so a value of 30 means the color changes once per second.
                 */
                set: function (newTicks) {
                    this._blinkTicks = newTicks;
                },
                enumerable: true,
                configurable: true
            });
            /**
             * Using the current text and font, determine exactly how wide and tall the current entity should be,
             * and reset the origin to be the center.
             */
            BlinkingText.prototype.resetDimensions = function () {
                // Get the renderer from the current stage, and from that the canvas context.
                var context = this._stage.renderer.context;
                // Now we need to save the state of the context, swap the font for our font, and measure the
                // text, then reset the context.
                context.save();
                context.font = this._font;
                this._width = context.measureText(this._text).width;
                context.restore();
                // Our height comes from our font, which should start with the font size in pixels for this to
                // work.
                this._height = parseInt(this._font, 10);
                // We want our origin to be at our center.
                this._origin.setToXY(this._width / 2, this._height / 2);
            };
            /**
             * This is invoked every frame to update this text.
             *
             * @param stage the stage the paddle is on
             * @param tick the game tick, for timing purposes
             */
            BlinkingText.prototype.update = function (stage, tick) {
                // Every 7 ticks (roughly every quarter second) change the color of the text
                if (tick % this._blinkTicks == 0)
                    this._blinkIndex = (this._blinkIndex + 1) % this._blinkColors.length;
            };
            /**
             * Render the text using the renderer provided. The position provided represents the actual position
             * of the text as realized on the screen, which may be different from its actual position if scrolling
             * or a view port of some sort is in use.
             *
             * @param x the x location to render the text at, in stage coordinates (NOT world)
             * @param y the y location to render the text at, in stage coordinates (NOT world)
             * @param renderer the class to use to render the text
             */
            BlinkingText.prototype.render = function (x, y, renderer) {
                // Translate the context so the origin is at the point we were given; this also saves the state of
                // the context. We don't need to take the entity origin into account here because we're centering
                // around the position provided below no matter what.
                renderer.translateAndRotate(x, y);
                // Alter the font and text alignment so that text is centered horizontally and vertically around
                // the given position.
                renderer.context.font = this._font;
                renderer.context.textAlign = "center";
                renderer.context.textBaseline = "middle";
                // Now render the text with the current color and then restore the context.
                renderer.drawTxt(this._text, 0, 0, this._blinkColors[this._blinkIndex]);
                renderer.restore();
            };
            return BlinkingText;
        }(game.Entity));
        game.BlinkingText = BlinkingText;
    })(game = nurdz.game || (nurdz.game = {}));
})(nurdz || (nurdz = {}));
var nurdz;
(function (nurdz) {
    var game;
    (function (game) {
        /**
         * The title of the game as displayed on the title screen.
         *
         * @type {String}
         */
        var TITLE_TEXT = "Super Brick Breaker";
        /**
         * The blinking text that tells the player how to play.
         *
         * @type {String}
         */
        var CLICK_TEXT = "Click to play";
        /**
         * The text that is used to introduce the high score, if one is set.
         *
         * @type {String}
         */
        var HIGH_SCORE_TEXT = "High Score: ";
        /**
         * The font used to render the title of the game.
         *
         * @type {String}
         */
        var TITLE_FONT = '48px kenvector_futureregular';
        /**
         * The font used to display the "click to play" message.
         *
         * @type {String}
         */
        var PLAY_FONT = '16px kenvector_futureregular';
        /**
         * The "score" font; this is the font that is used to display the high score
         * text (if any).
         *
         * @type {String}
         */
        var SCORE_FONT = '30px kenvector_futureregular';
        /**
         * The "normal" font; this is used for everything not outlined above.
         *
         * @type {String}
         */
        var NORMAL_FONT = '20px kenvector_futureregular';
        /**
         * All of the text that appears on the title screen, apart from the
         * notification that you should click to play, the high score, or the name
         * of the game.
         *
         * @type {Array<String>}
         */
        var infoText = [
            "A simple breakout clone",
            "",
            "Based on the course by Chris DeLeon (@ChrisDeleon)",
            "See http://how-to-program-games.com/ for more",
            "",
            "This version is \xA9 2016 Terence Martin (@OdatNurd)",
            "",
            "http://bloggity.nurdz.com/",
            "http://gamedev.nurdz.com/",
            "http://gitlab.com/u/OdatNurd/"
        ];
        /**
         * This scene represents the game screen, where the game is actually played.
         */
        var TitleScene = (function (_super) {
            __extends(TitleScene, _super);
            /**
             * Construct a new game screen scene that will display on the provided stage.
             *
             * @param stage the stage the scene will present on
             */
            function TitleScene(stage) {
                var _this = this;
                _super.call(this, "titleScreen", stage);
                /**
                 * Invoked when the image we want to use for our background is loaded; this sets up our member
                 * variable for the background.
                 *
                 * When the image provided is smaller than the stage, the image is tiled to a size that will fill the
                 * stage first.
                 *
                 * @param image The image that was loaded
                 */
                this.setBackgroundImg = function (image) {
                    // Create a tiles version of the coming image.
                    _this._backgroundImg = game.tileImage(_this._stage, image);
                };
                // Create a point that will store the mouse location.
                this._mouse = new game.Point(0, 0);
                // Some blinking text that tells the user how to start the game. We want this to be centered on
                // the screen a little ways from the bottom
                this._clickText = new game.BlinkingText(stage, PLAY_FONT, CLICK_TEXT);
                this._clickText.position.setToXY(stage.width / 2, stage.height - 25);
                // Add the item to the screen so that it will render properly.
                this.addActor(this._clickText);
                // Preload the image for the background; we don't assign this to an image directly, because after
                // loading we might need to tile the image.
                stage.preloadImage("blue_background_tile.png", this.setBackgroundImg);
                // Set up the gradient we use to display our title.
                this._gradient = this.makeTitleGradient();
            }
            /**
             * Create the gradient that will be used to display the title text. This uses the colors from the
             * _brickColors array and returns back a gradient that can be used to render the title text.
             *
             * Note that due to the way that gradients work in HTML canvas, this sets up the gradient to assume
             * that a translate to the center of the canvas is going to happen when the title text is rendered.
             *
             * @return {CanvasGradient} the gradient used to display the title text.
             */
            TitleScene.prototype.makeTitleGradient = function () {
                var titleWidth = 0;
                // A gradient covers a certain area of the canvas, so we need to determine how wide the title text
                // will be when we render it, so that we can make a gradient that covers the whole thing. We do
                // that by temporarily setting the title font so we can measure the text.
                this._renderer.context.save();
                this._renderer.context.font = TITLE_FONT;
                titleWidth = this._renderer.context.measureText(TITLE_TEXT).width;
                this._renderer.context.restore();
                // Now create the gradient itself. A linear gradient is represented by a line (specified in canvas
                // coordinates); anything to the left of the start point or the right of the end point is an
                // extrapolation of the gradient.
                //
                // An important note here is that the gradient position is always in canvas coordinates, even if
                // the canvas is translated. We want to center our title text when we render it which we do by
                // translating the origin to the center point horizontally.
                //
                // This has the unfortunate effect of making everything to the left of the center line be an
                // extension of the first color stop because it is less than 0 and thus outside the bounds of the
                // gradient.
                //
                // To get around that, when we define the gradient, we do it knowing that half of it's width will
                // be to the left of the center point and half of it to the right.
                var gradient = this._renderer.context.createLinearGradient(-(titleWidth / 2), 0, titleWidth / 2, 0);
                // Gradient color stops are set by specifying a position between 0.0 and 1.0. Determine how far
                // each color step in the gradient will be, as a percentage.
                var step = 1.0 / game.brickColors.length;
                // Loop setting in all of the color stops now. It is an error to specify a position that is larger
                // than 1.0, so we take care to make sure that the last gradient stop is always exactly at the 1.0
                // position, in case the number of color stops doesn't make this happen in a nicer way.
                for (var i = 0, position = 0.0; i < game.brickColors.length; i++, position += step) {
                    if (i == game.brickColors.length - 1)
                        position = 1.0;
                    // Add a color stop
                    gradient.addColorStop(position, game.brickColors[i]);
                }
                // Return the gradient now.
                return gradient;
            };
            /**
             * This gets invoked when our scene becomes the active scene; If the music is not muted and not
             * already playing, this starts it.
             *
             * @param previousScene the scene that used to be active.
             */
            TitleScene.prototype.activating = function (previousScene) {
                // Let the super do its thing.
                _super.prototype.activating.call(this, previousScene);
                // Set up what our font should be while this screen is active.
                this._renderer.context.font = NORMAL_FONT;
                // Play the title screen music if we're not muted.
                if (game.musicMute == false)
                    game.music[game.GameMusic.MUSIC_TITLE].play();
            };
            /**
             * This gets invoked when our scene is no longer going to be the active scene; stop our music from
             * playing.
             */
            TitleScene.prototype.deactivating = function (nextScene) {
                // Let the super do it's thing
                _super.prototype.deactivating.call(this, nextScene);
                // Stop our music if it's playing.
                if (game.music[game.GameMusic.MUSIC_TITLE].isPlaying)
                    game.music[game.GameMusic.MUSIC_TITLE].pause();
            };
            /**
             * This is triggered whenever the mouse is released after it was pressed over the canvas.
             *
             * @param  {MouseEvent} eventObj the event that represents the mouse release
             * @return {boolean} true if we handled the event or false if not
             */
            TitleScene.prototype.inputMouseUp = function (eventObj) {
                // When clicked, switch to the game scene to play the actual game.
                this._stage.switchToScene('game');
                return true;
            };
            /**
             * Triggers every time a key is pressed
             *
             * @param eventObj
             * @returns {boolean}
             */
            TitleScene.prototype.inputKeyDown = function (eventObj) {
                switch (eventObj.keyCode) {
                    // For the F key, toggle between full screen mode and windowed mode.
                    case game.KeyCodes.KEY_F:
                        this._stage.toggleFullscreen();
                        return true;
                    // For the M key, toggle our mute state, playing or stopping music as needed
                    case game.KeyCodes.KEY_M:
                        // Toggle the state and save the new value
                        game.musicMute = !game.musicMute;
                        game.saveMusicMute();
                        // If we're now muted, pause the music, otherwise play it.
                        game.music[game.GameMusic.MUSIC_TITLE].toggle();
                        return true;
                }
                // Let the default happen
                return _super.prototype.inputKeyDown.call(this, eventObj);
            };
            /**
             * Draw the text provided centered horizontally and vertically at the position given, using a given
             * font and color/gradient.
             *
             * This operates by making a translation of the canvas origin to be the location provided and then
             * render the text at (0, 0) after setting everything up. This is important to realize when using a
             * gradient.
             *
             * The color provided can be either a color spec string or it can be a CanvasGradient object. In the
             * latter case, the gradient is used as is. Due to the way that gradients work in HTML canvas, this
             * means that you probably want to have given the gradient a width that is the same as the width of
             * the text to be rendered AND that it's position starts to the left of the origin and continues to
             * the right.
             *
             * @param x     the x location to center around
             * @param y     the y location to center around
             * @param font  the font to use, including the size
             * @param text  the text to render
             * @param color the color to blit with OR the gradient to use to render (see above)
             */
            TitleScene.prototype.renderCenteredAt = function (x, y, font, text, color) {
                // Translate the context so the origin is at the point we were given; this also saves the state
                // of the context.
                this._renderer.translateAndRotate(x, y);
                // Alter the font and text alignment so that text is centered horizontally and vertically around
                // the given position/
                this._renderer.context.font = font;
                this._renderer.context.textAlign = "center";
                this._renderer.context.textBaseline = "middle";
                // If the color parameter is not a string, then it must be a gradient. In this case we will set
                // the fill style of the context and then set the color string to be undefined. This means that
                // the call to drawTxt() below will not set a text color, which would override the fill style.
                //
                // This should actually be fixed in the renderer, but no time for that now.
                if (typeof color != "string") {
                    this._renderer.context.fillStyle = color;
                    color = undefined;
                }
                // Render the text with the color given (if any) and then restore the context. Here we need to
                // use a typecast to tell the typescript compiler that color is a string.
                this._renderer.drawTxt(text, 0, 0, color);
                this._renderer.restore();
            };
            /**
             * This is called whenever our scene needs to be rendered.
             */
            TitleScene.prototype.render = function () {
                // Clear the screen with our background image.
                this._renderer.blit(this._backgroundImg, 0, 0);
                // Draw the title of the game.
                this.renderCenteredAt(this._stage.width / 2, 25, TITLE_FONT, TITLE_TEXT, this._gradient);
                // If there is a high score set, we want to display it now; this doesn't happen if we don't have
                // a current high score, since displaying that the high score is 0 is kind of weak.
                if (game.highScore != 0)
                    this.renderCenteredAt(this._stage.width / 2, 64, SCORE_FONT, HIGH_SCORE_TEXT + game.highScore, 'red');
                // Render the info text now; We will do a translate here to make things easier on us.
                this._renderer.translateAndRotate(32, 200);
                this._renderer.context.font = NORMAL_FONT;
                this._renderer.context.textBaseline = "middle";
                for (var i = 0, y = 0; i < infoText.length; i++, y += 24)
                    this._renderer.drawTxt(infoText[i], 0, y, 'white');
                this._renderer.restore();
                // Lastly, invoke the super method, which will render our entities.
                _super.prototype.render.call(this);
                // Render the current mute state now, on top of everything else.
                game.renderMuteState(this._stage);
            };
            return TitleScene;
        }(game.Scene));
        game.TitleScene = TitleScene;
    })(game = nurdz.game || (nurdz.game = {}));
})(nurdz || (nurdz = {}));
var nurdz;
(function (nurdz) {
    var game;
    (function (game) {
        /**
         * Every time a ball speed change happens, this is how much the ball speed gets kicked up as a percentage
         * of its current speed (values smaller than 1 slow it down while values of exactly 1 do nothing).
         */
        game.BALL_SPEED_INCREASE = 1.10;
        /**
         * The minimum speed of the ball to be considered for the multiplier; this corresponds to the speed the
         * ball must be traveling to earn MIN_SCORE_MULTIPLIER.
         */
        var MIN_MULTIPLIER_SPEED = 8;
        /**
         * The maximum speed of the ball to be considered for the multiplier; this corresponds to the speed the
         * ball must be traveling to earn MAX_SCORE_MULTIPLIR.
         */
        var MAX_MULTIPLIER_SPEED = 16;
        /**
         * The minimum score multiplier. This is the smallest multiplier that can be earned, and corresponds to
         * the MIN_MULTIPLIER speed.
         */
        var MIN_SCORE_MULTIPLIER = 1;
        /**
         * The maximum score multiplier. This is the largest multiplier that can be earned, and corresponds to
         * the MAX_MULTIPLIER speed.
         */
        var MAX_SCORE_MULTIPLIER = 5;
        /**
         * The maximum number of lives that can be displayed on the screen; any more than this are not displayed
         * because they would go off the side of the screen in an unfavorable way.
         */
        var MAX_LIVES_DISPLAYED = 8;
        /**
         * When one of the extra life icons is blinking, this is how many game ticks it takes for the display
         * to toggle states (there are 30 ticks per second).
         */
        var LIFE_BLINK_TICKS = 3;
        /**
         * How long (in ticks) to blink an extra life icon when that is turned on. There are 30 ticks per second.
         */
        var LIFE_BLINK_DURATION = 60;
        /**
         * This represents the different sounds that we know how to play based on events in the game. The values
         * in the enum are used as indexes into the array of sound effects at runtime.
         */
        var GameSoundItem;
        (function (GameSoundItem) {
            GameSoundItem[GameSoundItem["BALL_LOST"] = 0] = "BALL_LOST";
            GameSoundItem[GameSoundItem["EXTRA_BALL"] = 1] = "EXTRA_BALL";
            GameSoundItem[GameSoundItem["COLLIDE_WALL"] = 2] = "COLLIDE_WALL";
            GameSoundItem[GameSoundItem["COLLIDE_PADDLE"] = 3] = "COLLIDE_PADDLE";
            GameSoundItem[GameSoundItem["COLLIDE_BRICK"] = 4] = "COLLIDE_BRICK";
        })(GameSoundItem || (GameSoundItem = {}));
        ;
        /**
         * This scene represents the game screen, where the game is actually played.
         */
        var GameScene = (function (_super) {
            __extends(GameScene, _super);
            /**
             * Construct a new game screen scene that will display on the provided stage.
             *
             * @param stage the stage the scene will present on
             */
            function GameScene(stage) {
                var _this = this;
                _super.call(this, "gameScreen", stage);
                /**
                 * Invoked when the image we want to use for our background is loaded; this sets up our member
                 * variable for the background.
                 *
                 * When the image provided is smaller than the stage, the image is tiled to a size that will fill the
                 * stage first.
                 *
                 * @param image The image that was loaded
                 */
                this.setBackgroundImg = function (image) {
                    // Create a tiles version of the coming image.
                    _this._backgroundImg = game.tileImage(_this._stage, image);
                };
                // There is initially no blinking life icon.
                this.blinkLifeIcon = false;
                // Create a point that will store the mouse location.
                this._mouse = new game.Point(0, 0);
                // Create our entities now.
                this._ball = new game.Ball(stage);
                this._paddle = new game.Paddle(stage);
                this._brickGrid = new game.BrickGrid(stage);
                // Preload the image for the background; we don't assign this to an image directly, because after
                // loading we might need to tile the image.
                stage.preloadImage("blue_background_tile.png", this.setBackgroundImg);
                // Preload the image we use for our life icon.
                this._lifeIcon = stage.preloadImage("lifeIcon.png");
                // Preload all of our sounds; these are stored in an array.
                this._sounds = [];
                this._sounds[GameSoundItem.BALL_LOST] = stage.preloadSound("ball_lost");
                this._sounds[GameSoundItem.EXTRA_BALL] = stage.preloadSound("extra_life");
                this._sounds[GameSoundItem.COLLIDE_WALL] = stage.preloadSound("collide_wall");
                this._sounds[GameSoundItem.COLLIDE_PADDLE] = stage.preloadSound("collide_paddle");
                this._sounds[GameSoundItem.COLLIDE_BRICK] = stage.preloadSound("collide_brick");
                // Turn on debug mode for all of the entities so that we can verify that everything is working
                // the way we want it to.
                // this._ball.properties.debug = true;
                // this._paddle.properties.debug = true;
                // Add all entities as actors so that they get updated and rendered. Note that we add the
                // paddle first; this means that it gets updates and renders first, which means that it
                // renders below the ball but also that it moves before the ball moves.
                this.addActor(this._paddle);
                this.addActor(this._ball);
                this.addActor(this._brickGrid);
                // Set ourselves as the event listener for the ball, so we can tell when it gets lost.
                this._ball.listener = this;
            }
            Object.defineProperty(GameScene.prototype, "nextLifeScore", {
                /**
                 * Get the score at which the next life will be awarded
                 *
                 * @return the score at which the next life will be awarded.
                 */
                get: function () { return this._nextLifeScore; },
                /**
                 * Set the value of the score that will aware the next free life. This sets the member like you would
                 * expect it to but also pre-calculates the text version and where it should be displayed.
                 *
                 * @param newScore the new score to set as the free man target
                 */
                set: function (newScore) {
                    // First, set the next life score.
                    this._nextLifeScore = newScore;
                    // Now update the next life text and text position. This requires our renderer so that it can
                    // measure how wide the text will be in the current font.
                    //
                    // The 16 here is taken because that's how far to the right the score is displayed in the render
                    // method. I didn't make a constant for that due to laziness.
                    this._nextLifeText = "Next: " + this._nextLifeScore;
                    this._nextLifeTextPos = this._stage.width - 16 - this._renderer.context.measureText(this._nextLifeText).width;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(GameScene.prototype, "blinkLifeIcon", {
                /**
                 * Set the state of the flag that indicates if the first extra life icon (if any) should blink or not.
                 *
                 * Setting this to true sets up the blink to start at the next frame update and proceed for a set
                 * duration, while setting it to false stops the blink.
                 *
                 * @param newBlink true to turn on blinking and false to turn it off.
                 */
                set: function (newBlink) {
                    // First, set the main boolean, then set up as needed.
                    this._blinkLifeIcon = newBlink;
                    if (newBlink) {
                        this._blinkLifeDuration = LIFE_BLINK_DURATION;
                        this._blinkOn = true;
                    }
                },
                enumerable: true,
                configurable: true
            });
            /**
             * This gets invoked when our scene becomes the active scene; If the music is not muted and not
             * already playing, this starts it.
             *
             * @param previousScene the scene that used to be active.
             */
            GameScene.prototype.activating = function (previousScene) {
                // Let the super do its thing.
                _super.prototype.activating.call(this, previousScene);
                // Set up what our font should be while this screen is active.
                this._renderer.context.font = '30px kenvector_futureregular';
                // Reset the game for another play.
                this.resetGame();
                // Play the in-game music if we're not muted.
                if (game.musicMute == false)
                    game.music[game.GameMusic.MUSIC_GAME].play();
            };
            /**
             * This gets invoked when our scene is no longer going to be the active scene; stop our music from
             * playing.
             */
            GameScene.prototype.deactivating = function (nextScene) {
                // Let the super do it's thing
                _super.prototype.deactivating.call(this, nextScene);
                // Stop our music if it's playing.
                if (game.music[game.GameMusic.MUSIC_GAME].isPlaying)
                    game.music[game.GameMusic.MUSIC_GAME].pause();
            };
            /**
             * Reset everything that tracks the game state back to the initial state
             */
            GameScene.prototype.resetGame = function () {
                // Reset the score and lives.
                game.lives = 3;
                game.score = 0;
                // Set the initial score that an extra life will be earned at.
                this.nextLifeScore = game.EXTRA_LIFE_SCORE;
                // Reset the ball, make sure the paddle is visible, and then reset the brick grid.
                this.resetBall(this._ball);
                this._paddle.visible = true;
                this._brickGrid.reset(game.levelList[game.Utils.randomIntInRange(0, game.levelList.length - 1)]);
                // If the game somehow ended while an extra life was blinking, stop it now.
                this.blinkLifeIcon = false;
            };
            /**
             * Reset the ball, locking its position to the paddle and setting the lock offset so that the ball is
             * just touching the top edge of the paddle.
             *
             * This is used every time we want to reset the ball during the game (at game start or when a life is
             * lost) to allow the player a chance to control when and how the ball is released.
             */
            GameScene.prototype.resetBall = function (ball) {
                // Reset the ball, locking it to the paddle.
                //
                // While the ball is locked to the paddle, its position is relative to the paddle. Adjust its
                // position so that it is above the center of the paddle.
                ball.reset(8, this._paddle);
                ball.position.setToXY(0, -ball.radius - (this._paddle.height / 2));
            };
            /**
             * This is triggered whenever the mouse is moved over the canvas.
             *
             * @param eventObj the event that represents the mouse movement.
             * @returns {boolean} true if we handled this event or false if not.
             */
            GameScene.prototype.inputMouseMove = function (eventObj) {
                // Get the current mouse position.
                this._mouse = this._stage.calculateMousePos(eventObj, this._mouse);
                // Constrain the mouse position to the allowable position for the paddle. This keeps the paddle
                // fully on the screen with a buffer of the radius of the ball on the left and the right, so that
                // even if the ball is attached to the paddle at the outermost extreme edge, it still cannot pass
                // off the edge of the screen.
                var newX = game.Utils.clampToRange(this._mouse.x, (this._paddle.width / 2), this._stage.width - (this._paddle.width / 2));
                this._paddle.jumpTo(newX);
                // We handled it.
                return true;
            };
            /**
             * This is triggered whenever the mouse is released after it was pressed over the canvas.
             *
             * @param  {MouseEvent} eventObj the event that represents the mouse release
             * @return {boolean} true if we handled the event or false if not
             */
            GameScene.prototype.inputMouseUp = function (eventObj) {
                // If the ball is parented to the paddle, release it now. This will allow it to move as it wants
                // to.
                if (this._ball.parent = this._paddle)
                    this._ball.parent = null;
                return true;
            };
            /**
             * Triggers every time a key is pressed
             * @param eventObj
             * @returns {boolean}
             */
            GameScene.prototype.inputKeyDown = function (eventObj) {
                switch (eventObj.keyCode) {
                    // For the F key, toggle between full screen mode and windowed mode.
                    case game.KeyCodes.KEY_F:
                        this._stage.toggleFullscreen();
                        return true;
                    // For the M key, toggle our mute state, playing or stopping music as needed
                    case game.KeyCodes.KEY_M:
                        // Toggle the state and save the new value
                        game.musicMute = !game.musicMute;
                        game.saveMusicMute();
                        // If we're now muted, pause the music, otherwise play it.
                        game.music[game.GameMusic.MUSIC_GAME].toggle();
                        return true;
                }
                // Let the default happen
                return _super.prototype.inputKeyDown.call(this, eventObj);
            };
            /**
             * Given a particular ball speed, return back the score multiplier that should apply for that particular
             * ball speed. The idea is that the faster the ball is moving, the higher the score multiplier.
             *
             * @param ballSpeed the incoming ball speed
             *
             * @return {number} the multiplier to use. This is always an integral value in the range of
             * MIN_SCORE_MULTIPLIER to MAX_SCORE_MULTIPLIER, inclusive.
             */
            GameScene.prototype.multiplier = function (ballSpeed) {
                // Clamp the ball speed so that it falls into the range of speeds that we consider for the
                // multiplier.
                ballSpeed = game.Utils.clampToRange(ballSpeed, MIN_MULTIPLIER_SPEED, MAX_MULTIPLIER_SPEED);
                // Now we need to convert the range to be in the range of 1 to 5 as a multiplier. This little bit
                // of math does a linear conversion between the minimum and maximum speeds to the range of the
                // minimum and maximum multipliers. We convert to an integer so that there are no fractional
                // multipliers.
                return Math.floor(((ballSpeed - MIN_MULTIPLIER_SPEED) / (MAX_MULTIPLIER_SPEED - MIN_MULTIPLIER_SPEED)) *
                    (MAX_SCORE_MULTIPLIER - MIN_SCORE_MULTIPLIER) + MIN_SCORE_MULTIPLIER);
            };
            /**
             * Invoked once per game tick to update the frame and its contents.
             *
             * @param tick the current tick; increases by one for each invocation
             */
            GameScene.prototype.update = function (tick) {
                // If we are blinking the life icon, update state for it.
                if (this._blinkLifeIcon) {
                    // Decrement the duration; if it is done, turn the flag off. Otherwise, check and see if we
                    // should toggle.
                    this._blinkLifeDuration--;
                    if (this._blinkLifeDuration <= 0)
                        this._blinkLifeIcon = false;
                    else if (this._blinkLifeDuration % 7 == 0)
                        this._blinkOn = !this._blinkOn;
                }
                // Let the super update; this will shift the paddle to the new location and then allow the
                // ball to move. The ball will reset itself if it goes off the bottom or rebound off of the
                // walls if it can.
                _super.prototype.update.call(this, tick);
                // Check to see if the ball is colliding with any bricks. This call will remove any bricks
                // collided with and return the number of such removed bricks back, so when it's more than 0 we
                // can updaTe the score.
                var brickPos = this._ball.checkBrickCollision(this._brickGrid);
                if (brickPos != null) {
                    // We collided with a brick, so play the sound for that. This could also play a different
                    // sound for different bricks, or potentially the sound could be attached to the brick. Or
                    // both, and then a different sound plays altogether if the brick is not actually destroyed
                    // yet.
                    this._sounds[GameSoundItem.COLLIDE_BRICK].play(true);
                    // Get the brick at this location and decrement its life. If it's dead, then it is time to
                    // remove it and do scoring.
                    var brick = this._brickGrid.getBrick(brickPos.x, brickPos.y);
                    brick.life--;
                    if (brick.life <= 0) {
                        this._brickGrid.removeBrick(brickPos.x, brickPos.y);
                        // The ball speed gets kicked up a bit whenever it hits a pink brick or when it impacts a
                        // brick in every 4th row.
                        if (brick.color == game.BrickSpriteColor.PINK || brickPos.y % 4 == 0)
                            this._ball.speed *= game.BALL_SPEED_INCREASE;
                        // Add some points for the brick. The base 100 points is affected by a multiplier based on the
                        // speed of the ball (so a longer volley means more points).
                        game.score += ((100 * brick.maxLife) * this.multiplier(this._ball.speed));
                        // Time to earn an extra life?
                        if (game.score >= this._nextLifeScore) {
                            // We have earned an extra ball, so play the sound for that.
                            this._sounds[GameSoundItem.EXTRA_BALL].play();
                            // Add a life and make sure the user knows.
                            game.lives++;
                            this.blinkLifeIcon = true;
                            // Update the score for the next free life. We use the set property here so that the text
                            // version and location will be updated too.
                            this.nextLifeScore += game.EXTRA_LIFE_SCORE;
                        }
                    }
                }
                // Now we can check to see if the ball is colliding with the paddle.
                this._ball.checkPaddleCollision(this._paddle);
            };
            /**
             * Render the number of lives that are still left, as small paddle icons in the gutter below the
             * actual paddle.
             */
            GameScene.prototype.renderLives = function () {
                // Calculate a value that is a bit bigger than the width of the actual image we use to display
                // lives; this will be the offset between each when we render.
                var imgWidth = Math.floor(this._lifeIcon.width * 1.5);
                // Calculate the position where the first life icon will be rendered.
                var blitX = this._stage.width - imgWidth;
                var blitY = this._stage.height - (this._lifeIcon.height * 2);
                // Now we can loop and render all of the lives.
                for (var i = 0; i < game.lives && i < MAX_LIVES_DISPLAYED; i++, blitX -= imgWidth) {
                    // If this is the first life icon to render, AND we are blinking the life icon, AND it should
                    // not be visible, then skip the render here.
                    if (i == 0 && this._blinkLifeIcon == true && this._blinkOn == false)
                        continue;
                    this._renderer.blit(this._lifeIcon, blitX, blitY);
                }
            };
            /**
             * This is called whenever our scene needs to be rendered.
             */
            GameScene.prototype.render = function () {
                // Clear the screen with our background image.
                this._renderer.blit(this._backgroundImg, 0, 0);
                // Let the super render all of the entities now; this will display the ball, paddle and brick
                // grid.
                _super.prototype.render.call(this);
                // Now render lives and score. This makes sure that these items appear on top of everything
                // else, in case the ball is obstructing.
                this.renderLives();
                this._renderer.drawTxt("Score: " + game.score, 16, 32, 'white');
                this._renderer.drawTxt(this._nextLifeText, this._nextLifeTextPos, 32, 'red');
                // Render the current mute state now, on top of everything else.
                game.renderMuteState(this._stage);
            };
            /**
             * This gets invoked by the ball every time it goes off the bottom of the screen. We use this to
             * handle the ball being lost appropriately.
             *
             * @param {Ball} ball the ball that was lost.
             */
            GameScene.prototype.ballLost = function (ball) {
                // Play the sound for a ball being lost.
                this._sounds[GameSoundItem.BALL_LOST].play();
                // Make sure that the extra life icon stops blinking; we're about to use up the extra life.
                this.blinkLifeIcon = false;
                // Decrement the number of lives and check to see if the game is over or not.
                game.lives--;
                if (game.lives < 0) {
                    // If the current score is higher than the highest high score, set and save the high score
                    // into local storage (if possible) for the next time the page loads.
                    if (game.score > game.highScore) {
                        game.highScore = game.score;
                        game.saveHighScore();
                    }
                    // Make sure that the paddle is marked as not being visible and then switch to the game over
                    // scene and return. This makes sure that the paddle is not visible on the game over screen.
                    //
                    // The ball will be hidden because it should be marked as dead as part of the event that
                    // invoked us to tell us it was lost.
                    this._paddle.visible = false;
                    this._stage.switchToScene('gameOver');
                    return;
                }
                // Now we can reset the ball.
                this.resetBall(ball);
            };
            /**
             * This gets invoked by the ball whenever it collides with a paddle. This tells us which ball and
             * which paddle the collision happened between.
             *
             * Here we just need to play a sound because the ball handles it's reflection from the paddle on its
             * own.
             *
             * @param {Ball}   ball   the ball that collided with a paddle
             * @param {Paddle} paddle the paddle that the ball collided with
             */
            GameScene.prototype.ballPaddle = function (ball, paddle) {
                this._sounds[GameSoundItem.COLLIDE_PADDLE].play();
            };
            /**
             * This event triggers when a ball collides with a wall, causing it to be reflected back
             *
             * @param {Ball} ball the ball that collides with the wall
             */
            GameScene.prototype.ballWall = function (ball) {
                this._sounds[GameSoundItem.COLLIDE_WALL].play();
            };
            return GameScene;
        }(game.Scene));
        game.GameScene = GameScene;
    })(game = nurdz.game || (nurdz.game = {}));
})(nurdz || (nurdz = {}));
var nurdz;
(function (nurdz) {
    var game;
    (function (game) {
        /**
         * The font used to display the "press a key" message that restarts the game.
         *
         * @type {String}
         */
        var RESTART_FONT = '16px kenvector_futureregular';
        /**
         * The font used to render the game over text.
         *
         * @type {String}
         */
        var GAME_OVER_FONT = '64px kenvector_futureregular';
        /**
         * This scene represents the game screen, where the game is actually played.
         */
        var GameOverScene = (function (_super) {
            __extends(GameOverScene, _super);
            /**
             * Construct a new game screen scene that will display on the provided stage.
             *
             * @param stage the stage the scene will present on
             */
            function GameOverScene(stage, proxy) {
                _super.call(this, "gameOverScreen", stage);
                // Save the proxy scene we've been given.
                this._proxyScene = proxy;
                // Create some blinking text that tells us that the game is over.
                this._gameOver = new game.BlinkingText(stage, GAME_OVER_FONT, "Game Over");
                this._gameOver.position.setToXY(stage.width / 2, stage.height / 2);
                this._gameOver.colors = game.brickColors;
                this._gameOver.blinkTicks = 4;
                // Create our blinking text. We want it centered on the screen and a little ways up.
                this._pressText = new game.BlinkingText(stage, RESTART_FONT, "Press any key...");
                this._pressText.position.setToXY(stage.width / 2, stage.height - 25);
                // Add both to the scene now.
                this.addActor(this._gameOver);
                this.addActor(this._pressText);
            }
            /**
             * This gets invoked when our scene becomes the active scene; If the music is not muted and not
             * already playing, this starts it.
             *
             * @param previousScene the scene that used to be active.
             */
            GameOverScene.prototype.activating = function (previousScene) {
                // Let the super do its thing.
                _super.prototype.activating.call(this, previousScene);
                // Set up what our font should be while this screen is active.
                this._renderer.context.font = '30px kenvector_futureregular';
                // Play the game over music if we're not muted.
                if (game.musicMute == false)
                    game.music[game.GameMusic.MUSIC_GAMEOVER].play();
            };
            /**
             * This gets invoked when our scene is no longer going to be the active scene; stop our music from
             * playing.
             */
            GameOverScene.prototype.deactivating = function (nextScene) {
                // Let the super do it's thing
                _super.prototype.deactivating.call(this, nextScene);
                // Stop our music if it's playing.
                if (game.music[game.GameMusic.MUSIC_GAMEOVER].isPlaying)
                    game.music[game.GameMusic.MUSIC_GAMEOVER].pause();
            };
            /**
             * Triggers every time a key is pressed
             * @param eventObj
             * @returns {boolean}
             */
            GameOverScene.prototype.inputKeyDown = function (eventObj) {
                switch (eventObj.keyCode) {
                    // For the F key, toggle between full screen mode and windowed mode.
                    case game.KeyCodes.KEY_F:
                        this._stage.toggleFullscreen();
                        return true;
                    // For the M key, toggle our mute state, playing or stopping music as needed
                    case game.KeyCodes.KEY_M:
                        // Toggle the state and save the new value
                        game.musicMute = !game.musicMute;
                        game.saveMusicMute();
                        // If we're now muted, pause the music, otherwise play it.
                        game.music[game.GameMusic.MUSIC_GAMEOVER].toggle();
                        return true;
                    // Any other key jumps us back to the title screen.
                    default:
                        this._stage.switchToScene('title');
                        return true;
                }
                // Let the default happen
                // return super.inputKeyDown(eventObj);
            };
            /**
             * This is called whenever our scene needs to be rendered.
             */
            GameOverScene.prototype.render = function () {
                // Let the proxy render itself, which will give us a background and the remainder of the tiles,
                // but not the paddle and the ball because the game is over.
                this._proxyScene.render();
                // Now invoke the super method, which will make our own entities render.
                _super.prototype.render.call(this);
            };
            return GameOverScene;
        }(game.Scene));
        game.GameOverScene = GameOverScene;
    })(game = nurdz.game || (nurdz.game = {}));
})(nurdz || (nurdz = {}));
var nurdz;
(function (nurdz) {
    var game;
    (function (game) {
        /**
         * A handy dandy reference for what character code is used for what color.
         *
         * "1": BLACK
         * "0": WHITE
         * "R": RED
         * "O": ORANGE
         * "Y": YELLOW
         * "G": GREEN
         * "B": BLUE
         * "P": PINK
         */
        /**
         * This is a template of a level in the game and represents the fully
         * available grid size as currently defined. It is possible to have fewer
         * rows that what are seen here, but you probably want to make sure that
         * the column count remains the same.
         *
         * @type {BrickLayout}
         */
        var template = [
            ["1", "1", "1", "1", "1", "1", "1", "1", "1", "1"],
            ["1", "1", "1", "1", "1", "1", "1", "1", "1", "1"],
            ["1", "1", "1", "1", "1", "1", "1", "1", "1", "1"],
            ["1", "1", "1", "1", "1", "1", "1", "1", "1", "1"],
            ["1", "1", "1", "1", "1", "1", "1", "1", "1", "1"],
            ["1", "1", "1", "1", "1", "1", "1", "1", "1", "1"],
            ["1", "1", "1", "1", "1", "1", "1", "1", "1", "1"],
            ["1", "1", "1", "1", "1", "1", "1", "1", "1", "1"],
            ["1", "1", "1", "1", "1", "1", "1", "1", "1", "1"],
            ["1", "1", "1", "1", "1", "1", "1", "1", "1", "1"],
            ["1", "1", "1", "1", "1", "1", "1", "1", "1", "1"],
            ["1", "1", "1", "1", "1", "1", "1", "1", "1", "1"],
            ["1", "1", "1", "1", "1", "1", "1", "1", "1", "1"],
            ["1", "1", "1", "1", "1", "1", "1", "1", "1", "1"],
            ["1", "1", "1", "1", "1", "1", "1", "1", "1", "1"],
            ["1", "1", "1", "1", "1", "1", "1", "1", "1", "1"],
        ];
        var level1 = [
            [" ", " ", " ", " ", " ", " ", " ", " ", " ", " "],
            [" ", " ", " ", " ", " ", " ", " ", " ", " ", " "],
            [" ", " ", " ", " ", " ", " ", " ", " ", " ", " "],
            ["R", "R", "R", "R", "R", "R", "R", "R", "R", "R"],
            ["Y", "Y", "Y", "Y", "Y", "Y", "Y", "Y", "Y", "Y"],
            ["B", "B", "B", "B", "B", "B", "B", "B", "B", "B"],
            ["O", "O", "O", "O", "O", "O", "O", "O", "O", "O"],
            ["P", "P", "P", "P", "P", "P", "P", "P", "P", "P"],
            ["G", "G", "G", "G", "G", "G", "G", "G", "G", "G"],
        ];
        var level2 = [
            [" ", " ", " ", " ", " ", " ", " ", " ", " ", " "],
            [" ", " ", " ", " ", " ", " ", " ", " ", " ", " "],
            [" ", " ", "0", " ", " ", " ", " ", "0", " ", " "],
            [" ", "0", "B", "0", " ", " ", "0", "B", "0", " "],
            [" ", "0", "B", "0", " ", " ", "0", "B", "0", " "],
            [" ", "0", "1", "0", " ", " ", "0", "1", "0", " "],
            [" ", " ", "0", " ", " ", " ", " ", "0", " ", " "],
            [" ", " ", " ", " ", " ", " ", " ", " ", " ", " "],
            [" ", " ", " ", " ", " ", " ", " ", " ", " ", " "],
            [" ", "R", " ", " ", " ", " ", " ", " ", "R", " "],
            [" ", "R", "R", "R", "R", "R", "R", "R", "R", " "],
            [" ", "R", "R", "R", "R", "R", "R", "R", "R", " "],
            [" ", " ", "R", "R", "R", "R", "R", "R", " ", " "],
        ];
        var level3 = [
            [" ", " ", " ", " ", "B", "B", " ", " ", " ", " "],
            [" ", " ", " ", "B", "G", "G", "B", " ", " ", " "],
            [" ", " ", "B", "P", "P", "P", "P", "B", " ", " "],
            [" ", "B", "R", "R", "R", "R", "R", "R", "B", " "],
            ["B", "B", "B", "B", "B", "B", "B", "B", "B", "B"],
            ["O", "O", "O", "O", "O", "O", "O", "O", "O", "O"],
            [" ", "0", "0", "0", "0", "0", "0", "0", "0", " "],
            [" ", " ", "1", "1", "0", "0", "1", "1", " ", " "],
            [" ", " ", " ", "1", "0", "0", "1", " ", " ", " "],
            [" ", " ", " ", " ", "0", "0", " ", " ", " ", " "],
            [" ", " ", " ", " ", "0", "0", " ", " ", " ", " "],
        ];
        var level4 = [
            [" ", " ", " ", "R", " ", " ", " ", " ", " ", "R"],
            [" ", " ", "R", "P", "R", " ", " ", " ", "R", "P"],
            [" ", "R", "P", "0", "P", "R", " ", "R", "P", "0"],
            ["R", "P", "0", " ", "0", "P", "R", "P", "0", " "],
            ["P", "0", " ", " ", " ", "0", "P", "0", " ", " "],
            ["0", " ", " ", " ", " ", " ", "0", " ", " ", " "],
            [" ", " ", " ", "R", " ", " ", " ", " ", " ", "R"],
            [" ", " ", "R", "G", "R", " ", " ", " ", "R", "G"],
            [" ", "R", "G", "B", "G", "R", " ", "R", "G", "B"],
            ["R", "G", "B", " ", "B", "G", "R", "G", "B", " "],
            ["G", "B", " ", " ", " ", "B", "G", "B", " ", " "],
            ["B", " ", " ", " ", " ", " ", "B", " ", " ", " "],
        ];
        var level5 = [
            ["1", "1", "1", "1", "1", "1", "1", "1", "1", "1"],
            ["O", "R", "P", "Y", "1", "1", "Y", "P", "R", "O"],
            ["O", "R", "P", "Y", "1", "1", "Y", "P", "R", "O"],
            ["Y", "1", "O", "G", "1", "1", "G", "O", "1", "Y"],
            ["Y", "1", "O", "G", "1", "1", "G", "O", "1", "Y"],
            ["G", "1", "Y", "B", "1", "1", "B", "Y", "1", "G"],
            ["G", "1", "Y", "B", "1", "1", "B", "Y", "1", "G"],
            ["B", "1", "G", "P", "R", "R", "P", "G", "1", "B"],
            ["B", "1", "G", "P", "R", "R", "P", "G", "1", "B"],
            ["P", "1", "B", "O", "R", "R", "O", "B", "1", "P"],
            ["P", "1", "B", "O", "R", "R", "O", "B", "1", "P"],
            ["R", "1", "1", "1", "1", "1", "1", "1", "1", "R"],
        ];
        /**
         * This represents the list of all known levels, for selecting one at startup.
         *
         * @type {Array<BrickLayout>}
         */
        game.levelList = [
            level1,
            level2,
            level3,
            level4,
            level5
        ];
    })(game = nurdz.game || (nurdz.game = {}));
})(nurdz || (nurdz = {}));
var nurdz;
(function (nurdz) {
    var main;
    (function (main) {
        /**
         * Set up the button on the page to toggle the state of the game.
         *
         * @param stage the stage to control
         * @param buttonID the ID of the button to mark up to control the game state
         */
        function setupButton(stage, buttonID) {
            // True when the game is running, false when it is not. This state is toggled by the button. We
            // assume that the game is going to start running.
            var gameRunning = true;
            // Get the button.
            var button = document.getElementById(buttonID);
            if (button == null)
                throw new ReferenceError("No button found with ID '" + buttonID + "'");
            // Set up the button to toggle the stage.
            button.addEventListener("click", function () {
                // Try to toggle the game state. This will only throw an error if we try to put the game into
                // a state it is already in, which can only happen if the engine stops itself when we didn't
                // expect it.
                try {
                    if (gameRunning) {
                        stage.muteMusic(true);
                        stage.muteSounds(true);
                        stage.stop();
                    }
                    else {
                        stage.muteMusic(false);
                        stage.muteSounds(false);
                        stage.run();
                    }
                }
                // Log and then re-throw the error.
                catch (error) {
                    console.log("Exception generated while toggling game state");
                    throw error;
                }
                finally {
                    // No matter what, toggle the state.
                    gameRunning = !gameRunning;
                    button.innerHTML = gameRunning ? "Stop Game" : "Restart Game";
                }
            });
        }
        // Once the DOM is loaded, set things up.
        nurdz.contentLoaded(window, function () {
            try {
                // Set up the stage.
                var stage = new nurdz.game.Stage('gameContent', 'black', true, '#a0a0a0');
                // Set up the default values used for creating a screen shot.
                nurdz.game.Stage.screenshotFilenameBase = "ts-breakout";
                nurdz.game.Stage.screenshotWindowTitle = "ts-breakout";
                // Set up the button that will stop the game if something goes wrong.
                setupButton(stage, "controlBtn");
                // Preload all of our music values.
                nurdz.game.music = [];
                nurdz.game.music[nurdz.game.GameMusic.MUSIC_TITLE] = stage.preloadMusic("PopcornSliderLoop");
                nurdz.game.music[nurdz.game.GameMusic.MUSIC_GAME] = stage.preloadMusic("Ouroboros");
                nurdz.game.music[nurdz.game.GameMusic.MUSIC_GAMEOVER] = stage.preloadMusic("Mellowtron");
                // Set up the sprite sheet for the global speaker icon.
                nurdz.game.speakerSheet = new nurdz.game.SpriteSheet(stage, "speaker_2_1.png", 2, 1, true);
                // Try to set the high score and the music mute state to their last stored value; these will end
                // up with default values (e.g. 0 or false) if there is currently no stored value or if the local
                // storage mechanism is not enabled/present.
                nurdz.game.highScore = nurdz.game.loadHighScore();
                nurdz.game.musicMute = nurdz.game.loadMusicMute();
                // Register all of our scenes.
                stage.addScene("title", new nurdz.game.TitleScene(stage));
                var gameScene = new nurdz.game.GameScene(stage);
                stage.addScene("game", gameScene);
                stage.addScene("gameOver", new nurdz.game.GameOverScene(stage, gameScene));
                // Switch to the initial scene and run the game.
                stage.switchToScene("title");
                stage.run();
            }
            catch (error) {
                console.log("Error starting the game");
                throw error;
            }
        });
    })(main = nurdz.main || (nurdz.main = {}));
})(nurdz || (nurdz = {}));
